<?php
/**
 * Plugin Name: Etsy360 Extended Plugin
 * Plugin URI: http://www.etsy360.com
 * Description: Etsy360 plugin allows Etsy shop owners to access their shop listings and other related shop information to place on their WordPress site
 * Author: VivioSoft
 * Author URI: http://www.viviosoft.com
 * Version: 3.2.0
 * Requires at least: 3.6
 * Tested up to: 4.6
 * Text Domain: Etsy360 Extended Plugin
 * @package Etsy360 Extended Plugin
 */

/*
 *
 */

// Exit if accessed directly
if (!defined('ABSPATH')) exit;

if (!class_exists('Etsy360')) :

    /**
     * Main Class
     */
    final class Etsy360
    {

        private static $instance;

        public static function instance()
        {
            if (!isset(self::$instance) && !(self::$instance instanceof Etsy360)) {

                self::$instance = new Etsy360;
                self::$instance->setup_constants();
                self::$instance->includes();
                self::$instance->oauth_table();

                add_action('admin_notices', array(__CLASS__, 'oauth_admin_notice'));
                add_action('admin_notices', array(__CLASS__, 'oauth_plugin_notice'));
//                add_action('admin_notices', array(__CLASS__, 'php_version_notification'));

                add_action('wp_head', array(__CLASS__, 'e360_ajax'));

                add_filter('https_local_ssl_verify', '__return_false');
                add_filter('https_ssl_verify', '__return_false');

//                add_filter('wp_title', 'etsy360_filter_wp_title', 10, 2);

                // This is a fix for IE and FireFox browsers
//                add_action('admin_init', array(__CLASS__, 'e360_init_caching'));

//                add_action('wp_ajax_nopriv_cache_shop', array(__CLASS__, 'cache_shop_callback'));
//                add_action('wp_ajax_cache_shop', array(__CLASS__, 'cache_shop_callback'));

//                register_activation_hook(__FILE__, array(__CLASS__, 'oauth_table'));

            }

            return self::$instance;

        }

        function etsy360_filter_wp_title($title, $sep)
        {

            if ($_GET['item-details']) {
                $etsy360Data = new Etsy_API();
                $itemListingDetails = $etsy360Data->getListingDetails($_GET['item-details']);
                $title = "$title  $sep " . sprintf(__($itemListingDetails[0]->title));
            } else {
                $title = 'Shop';
            }

            return $title;
        }

        private function setup_constants()
        {

            if (!defined('E360_STORE_URL')) {
                define('E360_STORE_URL', 'http://etsy360.com');
            }

            if (!defined('E360_ITEM_NAME')) {
                define('E360_ITEM_NAME', 'Etsy WordPress Shop Plugin');
            }

            if (!defined('E360_HOME_URL')) {
                define('E360_HOME_URL', home_url());
            }

            // Plugin version
            if (!defined('E360_VERSION')) {
                define('E360_VERSION', '3.2.0');
            }

            // Plugin Folder Path
            if (!defined('E360_PLUGIN_DIR')) {
                define('E360_PLUGIN_DIR', plugin_dir_path(__FILE__));
            }

            // Plugin Folder URL
            if (!defined('E360_PLUGIN_URL')) {
                define('E360_PLUGIN_URL', plugin_dir_url(__FILE__));
            }

            // Plugin Root File
            if (!defined('E360_PLUGIN_FILE')) {
                define('E360_PLUGIN_FILE', __FILE__);
            }

            if (!defined('PHP_VER_REQUIRED')) {
                define('PHP_VER_REQUIRED', '5.5.0');
            }


        }

        /**
         * Include required files
         *
         * @access private
         * @since 1.4
         * @return void
         */
        static function includes()
        {

            global $e360_options;

            require_once(ABSPATH . 'wp-config.php');

            require_once E360_PLUGIN_DIR . 'includes/class-helpers.php';
            require_once E360_PLUGIN_DIR . 'includes/class-debugger.php';
            require_once E360_PLUGIN_DIR . 'includes/class-etsy-api.php';

            if (is_admin()) {

                // New @since 2.4
                require_once(E360_PLUGIN_DIR . 'includes/admin/register-settings.php');

                $e360_options = e360_get_settings();

                require_once(ABSPATH . 'wp-admin/includes/plugin.php');
                require_once(ABSPATH . 'wp-admin/includes/file.php');

                require_once E360_PLUGIN_DIR . 'includes/admin/class-menu.php';
                require_once(E360_PLUGIN_DIR . 'includes/admin/display-settings.php');
                require_once(E360_PLUGIN_DIR . 'includes/class-license-handler.php');

                if (class_exists('E360_License')) {
                    $e360_license = new E360_License(__FILE__, E360_ITEM_NAME, E360_VERSION, 'VivioSoft', 'e360_license_key');
                }

            }

            if (class_exists('Etsy360_Helpers')) {
                $Etsy360_Helpers = new Etsy360_Helpers();
            }

            require_once E360_PLUGIN_DIR . 'includes/etsy360-scripts.php';
            require_once E360_PLUGIN_DIR . 'includes/class-pagination.php';

            require_once E360_PLUGIN_DIR . 'includes/class-shop-sections-widget.php';
            require_once E360_PLUGIN_DIR . 'includes/class-shop-listings-shortcode.php';
            require_once E360_PLUGIN_DIR . 'includes/class-shop-sections-shortcode.php';
            require_once E360_PLUGIN_DIR . 'includes/class-shop-featured-listings-shortcode.php';
            require_once E360_PLUGIN_DIR . 'includes/class-add-meta-box.php';
            require_once E360_PLUGIN_DIR . 'includes/lang.php';

        }

        function e360_ajax()
        {
            ?>
            <script type="text/javascript">
                var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
            </script>
            <?php
        }

        function oauth_table()
        {

            require_once(ABSPATH . 'wp-admin/includes/upgrade.php');

            global $wpdb;

            $table_name = $wpdb->prefix . 'etsy_oauth';
            $charset_collate = $wpdb->get_charset_collate();

            $sqlOauth = "CREATE TABLE IF NOT EXISTS $table_name (
                  id mediumint(10) NOT NULL AUTO_INCREMENT,
                  session varchar(32) NOT NULL DEFAULT '',
                  state varchar(32) NOT NULL DEFAULT '',
                  access_token mediumtext NOT NULL,
                  expiry datetime DEFAULT NULL,
                  type varchar(12) NOT NULL DEFAULT '',
                  server varchar(12) NOT NULL DEFAULT '',
                  creation datetime NOT NULL DEFAULT '2000-01-01 00:00:00',
                  access_token_secret mediumtext NOT NULL,
                  authorized varchar(1) DEFAULT NULL,
                  login_url varchar(1000) DEFAULT NULL,
                  user int(10) unsigned NOT NULL,
                  PRIMARY KEY (id),
                  UNIQUE KEY social_oauth_session_index (session,server)
                 ) $charset_collate;";

            dbDelta($sqlOauth);

        }

        function oauth_admin_notice()
        {

            global $wpdb;
            $table_name = $wpdb->prefix . 'etsy_oauth';
            $db_results = $wpdb->get_row("SELECT id, session, state, access_token, access_token_secret, expiry, authorized, type, server, creation, user FROM " . $table_name . " WHERE user = '1'");

            $url = site_url() . '/wp-admin/admin.php?page=etsy360-options&tab=general';

            /* Check that the user hasn't already clicked to ignore the message */
            if (empty($db_results)) {
                echo '<div class="admin_error"><p>';
                printf(__('Etsy360 needs attention.  Please connect to your Etsy OAuth. <a href="' . $url . '">Fix Now.</a>'), '');
                echo "</p></div>";
            }
        }

//        function php_version_notification()
//        {
//            if (version_compare(PHP_VERSION, PHP_VER_REQUIRED, '<')) {
//                echo '<div class="admin_error"><p>';
//                printf(__("Etsy360 Needs Attention - Your PHP version is " . PHP_VERSION . ". Please contact your hosting provider and have them upgraded to version 5.5+ Etsy360 may not operate correctly with the version you are currently running. "), '');
//                echo "</p></div>";
//            }
//        }

        function oauth_plugin_notice()
        {
            if (!class_exists('Etsy360OAuth\Etsy360_oAuth')) {
                echo '<div class="admin_error"><p>';
                printf(__('Be sure you have the Etsy360 OAuth Plugin enabled or your shop will not work.'), '');
                echo "</p></div>";
            }
        }

    }

endif; // End if class_exists check


function Etsy360_Plugin()
{
    return Etsy360::instance();
}

// Get Plugin Running
Etsy360_Plugin();