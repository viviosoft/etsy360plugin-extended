(function ($) {

    $.fn.mioValidationPlugin = function (sOptions) {

        var defaults = {
            formSelectors: null,
            formRules: null,
            showErrorMsg: false,
            formMessage: null,
            msgBackgroundColor: "#fef3f3",
            msgTextColor: "#000000",
            msgOpacity: ".8",
            msgPositionTop: "-38px",
            msgPositionRight: "-130px",
            showMark: false,
            markPositionTop: "-43px",
            markPositionLeft: "-6px"
        };

        var s_options = $.extend(defaults, sOptions);

        jQuery.each($(s_options.formSelectors)[0].elements,

            function (i, o) {

                var _this = jQuery(o);

                if (!_this.attr('data-validate')) {

                    _this.wrap('<span class="relative">').after('<span class="required"></span>' +
                        '<label for="' + _this.attr('id') + '" class="ValidateError" generated="true"></label>');

                    jQuery(".error").css({
                        "background-color": s_options.msgBackgroundColor,
                        "color": s_options.msgTextColor,
                        "opacity": s_options.msgOpacity
                    });

                }

            });


        jQuery(s_options.formSelectors).validate({
            errorPlacement: function (error, element) {
            },
            ignore: [],
            errorLabelContainer: '.errorsMessage',
            onsubmit: true,
            messages: s_options.formMessage,
            rules: s_options.formRules,
            highlight: function (element) {
                if (!s_options.showErrorMsg) {
                    jQuery(element).next().next().remove(".ValidateError");
                } else {
                    jQuery(element).next().next().fadeIn("slow");
                    jQuery(element).next().next().css({
                        "top": s_options.msgPositionTop,
                        "right": s_options.msgPositionRight
                    })
                }
                if (s_options.showMark) {
                    jQuery(element).next().addClass("check-error fi-x size-24");
                    jQuery(element).next().css({
                        "margin-top": s_options.markPositionTop,
                        "left": s_options.markPositionLeft
                    })
                }
            },
            unhighlight: function (element) {
                jQuery(element).next().removeClass('check-error fi-x size-24');
                jQuery(element).next().addClass("check-ok fi-check size-24");
            }

        });

        // Custom validators -- Phone Number

        jQuery.validator.addMethod("phoneUS", function (phone_number, element) {
            phone_number = phone_number.replace(/\s+/g, "");
            return this.optional(element) || phone_number.length > 9 &&
                phone_number.match(/^(1-?)?(\([2-9]\d{2}\)|[2-9]\d{2})-?[2-9]\d{2}-?\d{4}$/);
        }, "Please specify a valid phone number");


        // Validate for 2 decimal for money
        jQuery.validator.addMethod("price", function (value, element) {
            return this.optional(element) || /^(\d{1,3})(\.\d{1,2})?$/.test(value);
        }, "Must be in US currency format 0.99");

    }

})(jQuery);



