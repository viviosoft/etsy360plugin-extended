/**
*
* jQuery Color Picker Plugin
* URL: http://www.codecanyon.net/user/bamdaa
* Version: 1.2.1
* Author: BamDaa
* Author URL: http://www.codecanyon.net/user/bamdaa
*
*/

if( typeof Object.create !== 'function'){
	Object.create = function( obj ){
		function F(){};
		F.prototype = obj;
		return new F();
	};
}

(function( $, window, document, undefined ){
	
	var CPicker = {
		init: function( options, elem ){
			var self = this;
			self.elem = elem;
			self.$elem = $( elem );

			self.options = $.extend( {}, $.fn.colorPicker.options, options );


			if(typeof self.options.colors == "string")
			{
				
				$.ajax({
					dataType: "json",
					url: self.options.colors,
					success: function(data) {

						self.options.colors = data;
						self.build();						

					}
				}).error(function(){ alert("Can't load "+ self.options.colors +" file."); });

			}
			else{

				self.build();

			}

			

			self.$elem.bind("click", function(evt){

				evt.preventDefault();

				if(self.pWindow.attr("data-picker-open") == "false"){
					$(".bmd-picker-window").attr("data-picker-open", "false").fadeOut();
					self.pWindow.attr("data-picker-open", "true");
					self.setPosition();
					self.pWindow.show();
				}
				else{
					self.pWindow.attr("data-picker-open", "false");
					self.pWindow.fadeOut();
				}
			});
		},
		
		build: function(e){
			var self = this;
				self.pWindow 		= $(self.template.window);
				self.pColorList 	= $(self.template.list);

			if(self.options.customColors.length > 0){
				self.options.colors = self.options.colors.concat(self.options.customColors);
			}

			if(self.$elem.attr("data-custom-colors")){
				var customColors = eval(self.$elem.data("custom-colors"));
				self.options.colors = self.options.colors.concat(customColors);
			}

			$.each(self.options.colors, function(key, val){				
				var pItem = $(self.template.item);

				if(key > 0 && (key)%self.options.rowItem == 0)
					self.pColorList.append('<li class="split-color-row"></li>');

				pItem.find("a").css("background", val).attr("href", val).data("color", val).bind("click", function(e){
					e.preventDefault();
					self.options.onSelect(self.$elem, $(this).data("color"));						
					self.pWindow.attr("data-picker-open", "false");
					self.pWindow.fadeOut();
				});

				pItem.appendTo(self.pColorList);				
			});

			self.pColorList.appendTo(self.pWindow);

			var insertForm = 0;
			if(self.options.insertCode == 1){
				if(self.$elem.attr("data-insert-code") != "0"){
					insertForm = 1;
				}
			}
			else{
				if(self.$elem.attr("data-insert-code") == "1"){
					insertForm = 1;
				}
			}

			if(insertForm == 1){
				self.newColor = $(self.template.form);

				self.newColor.find("#hexColorButton").bind("click", function(){
					var newHEXColor = self.newColor.find("#hexColorInput").val();
					self.options.onSelect(self.$elem, newHEXColor);
					self.pWindow.attr("data-picker-open", "false");
					self.pWindow.fadeOut();
				});
				self.newColor.appendTo(self.pWindow);
			}

			self.pWindow.appendTo("body");
			self.pickerIs = false;

			$(document).mouseup(function(e){
				if(!self.pWindow.is(e.target) && !self.$elem.is(e.target) && self.pWindow.has(e.target).length === 0){
					self.pWindow.attr("data-picker-open", "false");
					self.pWindow.fadeOut();
				}
			});
		},

		setPosition: function(){
			var self 	= this;
			var height 	= self.elem.offsetHeight;
			var eLeft  	= self.$elem.offset().left;
			var eTop   	= self.$elem.offset().top;
			self.pWindow.css({left: eLeft, top: eTop + height + 2});
		},

		template:{
			window: 	'<div class="bmd-picker-window" data-picker-open="false"></div>',
			list: 		'<ul class="bmd-color-list"></ul>',
			item: 		'<li><a href=""></a></li>',
			form: 		'<div class="hexColorForm"><input type="text" id="hexColorInput" placeholder="#FF6600"/><input type="button" id="hexColorButton" value="Set" /></div>'
		}
	};
	
	$.fn.colorPicker = function( options ){
		return this.each(function(){
			var myCPicker = Object.create( CPicker );
			myCPicker.init( options, this );
		});
	};
	
	$.fn.colorPicker.options = {
		colors: [
			'#ffffff', '#000000', '#eeece1', '#1f497d', '#4f81bd', '#c0504d', '#9bbb59', '#8064a2', '#4bacc6', '#f79646', '#ffff00',
			'#f2f2f2', '#7f7f7f', '#ddd9c3', '#c6d9f0', '#dbe5f1', '#f2dcdb', '#ebf1dd', '#e5e0ec', '#dbeef3', '#fdeada', '#fff2ca',
			'#d8d8d8', '#595959', '#c4bd97', '#8db3e2', '#b8cce4', '#e5b9b7', '#d7e3bc', '#ccc1d9', '#b7dde8', '#fbd5b5', '#ffe694',
			'#bfbfbf', '#3f3f3f', '#938953', '#548dd4', '#95b3d7', '#d99694', '#c3d69b', '#b2a2c7', '#b7dde8', '#fac08f', '#f2c314',
			'#a5a5a5', '#262626', '#494429', '#17365d', '#366092', '#953734', '#76923c', '#5f497a', '#92cddc', '#e36c09', '#c09100',
			'#7f7f7f', '#0c0c0c', '#1d1b10', '#0f243e', '#244061', '#632423', '#4f6128', '#3f3151', '#31859b', '#974806', '#7f6000'
			],
		customColors: [],
		insertCode: 0,
		rowItem: 11,
		onSelect: function(ui, color){ console.log(color); }
	};

})(jQuery, window, document);