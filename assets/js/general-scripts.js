jQuery(function () {

    jQuery("img.crop").responsiveImageCropper();

    jQuery('#reset_oauth').click(function (e) {
        if (confirm('Are you sure you want to reset?  Doing so will require you to have to reconnect your Etsy OAuth session.')) {
            return true;
        }
        e.preventDefault();
    });

    jQuery('#cache-btn').on('click', function(e){

        jQuery("#loading-container").show();

        //var data = {
        //    'action': 'cache_shop'
        //};
        //
        //// since 2.8 ajaxurl is always defined in the admin header and points to admin-ajax.php
        //jQuery.post(ajaxurl, data, function (response) {
        //    console.log('Got this from the server: ' + response);
        //});
        //
        //e.preventDefault();

    });

    // Handle image thumbnails
    jQuery('#thumbs').delegate('img', 'click', function () {
        jQuery('#largeImage').attr('src', jQuery(this).attr('src').replace('75x75', 'fullxfull'));
    });

    jQuery(".getCode").colorPicker({
        insertCode: 1,
        onSelect: function (ui, color) {
            jQuery("#colorCode").val(color);
            jQuery("#colorCode").css('border-color', color);
            ui.css("background", color);
        }
    });

    jQuery('.etabs .etab-links a').on('click', function (e) {

        var currentAttrValue = jQuery(this).attr('href');

        // Show/Hide Tabs
        jQuery('.etabs ' + currentAttrValue).show().siblings().hide();

        // Change/remove current tab to active
        jQuery(this).parent('li').addClass('active').siblings().removeClass('active');

        e.preventDefault();
    });


    jQuery(".insertShortCode").on('click', function () {
        var shortCodeVal = jQuery(this).attr('sortCode');
        insert_shortCode(shortCodeVal);
    });

    jQuery('#largeImage').on('click', function (e) {

        jQuery.fancybox.showLoading();

        jQuery(".fancybox")
            .fancybox({
                helpers: {
                    title: {
                        type: 'inside'
                    }
                },
                nextEffect: 'fade',
                prevEffect: 'fade'
            });

    });

});


function insert_shortCode(shortcode_val) {
    if (tinyMCE && tinyMCE.activeEditor) {
        tinyMCE.activeEditor.selection.setContent(shortcode_val);
    }
    return false;
}



(function(jQuery){
    var ResponsiveImageCropper = function() {};
    ResponsiveImageCropper.prototype = {

        /**
         * target img element
         */
        targetElements: undefined,

        /**
         * options.
         */
        options: undefined,

        /**
         * run
         */
        run: function(targetElements){
            var _this = this;
            this.targetElements = new Array();
            targetElements.each(function(index){
                var imgElement = jQuery(this);
                imgElement.css({
                    display: "none"
                })

                var preload = new Image();

                preload.onload = function(){
                    imgElement.css({
                        position: "absolute"
                    });
                    _this.targetElements.push(imgElement);
                    _this.croppingImageElement(imgElement);
                    imgElement.css({
                        display: "block"
                    });
                }
                preload.src = imgElement.attr('src');
            });

            jQuery(window).resize(function(event){
                _this.onResizeCallback();
            });
        },

        /**
         * On resize callback function.
         */
        onResizeCallback: function(){
            var _this = this;
            jQuery.each(this.targetElements, function(index){
                var imgElement = this;
                _this.croppingImageElement(imgElement);
            });
        },

        /**
         * crop image
         */
        croppingImageElement: function(imgElement){
            var inner, outer;

            if (imgElement.data('crop-image-wrapped')){
                outer = imgElement.data('crop-image-outer');
                inner = imgElement.data('crop-image-inner');

            }else{
                outer = jQuery("<div>");
                inner = jQuery("<div>");
                outer.css({
                    overflow: "hidden",
                    margin: imgElement.css("margin"),
                    padding: imgElement.css("padding")
                });

                imgElement.css({
                    margin: 0,
                    padding: 0
                });

                inner.css({
                    position: "relative",
                    overflow: "hidden"
                });

                // append elements.
                imgElement.after(outer);
                outer.append(inner);
                inner.append(imgElement);

                // set datas.
                imgElement.data('crop-image-outer', outer);
                imgElement.data('crop-image-inner', inner);
                imgElement.data('crop-image-wrapped', true);
            }

            this.desideImageSizes(imgElement);
        },

        /**
         * Deside image width.
         */
        desideImageSizes: function(imgElement){
            var outer = imgElement.data('crop-image-outer');
            var inner = imgElement.data('crop-image-inner');
            var ratio = imgElement.data('crop-image-ratio');

            if (!ratio){
                ratio = 1;
            }

            var height = outer.width() * ratio;
            inner.height(height);

            imgElement.width(outer.width());
            imgElement.height('auto');
            imgElement.css({
                position: "absolute",
                left: 0,
                top: - (imgElement.height() - outer.height()) /2
            });

            if ( height > imgElement.height() ){
                imgElement.width('auto');
                imgElement.height(height);
                imgElement.css({
                    position: "absolute",
                    left: - (imgElement.width() - outer.width()) /2,
                    top: 0
                });
            }
        },

        /**
         * set options.
         */
        setOptions: function(options){
            this.options = options;
        }
    };

    /**
     * Main stream
     */
    jQuery.fn.responsiveImageCropper = function(options){
        var options = jQuery.extend(jQuery.fn.responsiveImageCropper.defaults, options);
        // set using objects.
        var targetElements = jQuery(this);

        // Event start
        cropper = new ResponsiveImageCropper();
        cropper.setOptions(options);
        cropper.run(targetElements);

        return this;
    };

    /**
     * default options.
     * @property screenFilterImage string put on top-screen image using `repeat-x left bottom`.
     */
    jQuery.fn.responsiveImageCropper.defaults = {

    };
})(jQuery);


/**
 * bl-jquery-image-center jQuery Plugin
 *
 * @copyright Boxlight Media Ltd. 2012
 * @license MIT License
 * @description Centers an image by moving, cropping and filling spaces inside it's parent container. Call
 * this on a set of images to have them fill their parent whilst maintaining aspect ratio
 * @author Robert Cambridge
 *
 * Usage: See documentation at http://boxlight.github.com/bl-jquery-image-center
 */
(function(jQuery) {
    jQuery.fn.centerImage = function(method, callback) {
        callback = callback || function() {};
        var els = this;
        var remaining = jQuery(this).length;
        method = method == 'inside';

        // execute this on an individual image element once it's loaded
        var fn = function(img) {
            var $img = jQuery(img);
            var $div = $img.parent();
            // parent CSS should be in stylesheet, but to reinforce:
            $div.css({
                overflow: 'hidden',
                position: $div.css('position') == 'absolute' ? 'absolute' : 'relative'
            });

            // temporarily set the image size naturally so we can get the aspect ratio
            $img.css({
                'position':   'static',
                'width':      'auto',
                'height':     'auto',
                'max-width':  '100%',
                'max-height': '100%'
            });

            // now resize
            var div = { w: $div.width(), h: $div.height(), r: $div.width() / $div.height() };
            var img = { w: $img.width(), h: $img.height(), r: $img.width() / $img.height() };
            $img.css({
                'max-width':  'none',
                'max-height': 'none',
                'width':      Math.round((div.r > img.r) ^ method ? '100%' : div.h / img.h * img.w),
                'height':     Math.round((div.r < img.r) ^ method ? '100%' : div.w / img.w * img.h)
            });

            // now center - but portrait images need to be centered slightly above halfway (33%)
            var div = { w: $div.width(), h: $div.height() };
            var img = { w: $img.width(), h: $img.height() };
            $img.css({
                'position': 'absolute',
                'left':     Math.round((div.w - img.w) / 2),
                'top':      Math.round((div.h - img.h) / 3)
            });

            callbackWrapped(img)
        };

        var callbackWrapped = function(img) {
            remaining--;
            callback.apply(els, [ img, remaining ]);
        };

        // iterate through elements
        return els.each(function(i) {
            if (this.complete || this.readyState === 'complete') {
                // loaded already? run fn
                // when binding, we can tell whether image loaded or not.
                // not if it's already failed though :(
                (function(el) {
                    // use setTimeout to prevent browser locking up
                    setTimeout(function() { fn(el) }, 1);
                })(this);
            } else {
                // not loaded? bind to load
                (function(el) {
                    jQuery(el)
                        .one('load', function() {
                            // use setTimeout to prevent browser locking up
                            setTimeout(function() {
                                fn(el);
                            }, 1);
                        })
                        .one('error', function() {
                            // the image did not load
                            callbackWrapped(el)
                        })
                        .end();

                    // IE9/10 won't always trigger the load event. fix it.
                    if (navigator.userAgent.indexOf("Trident/5") >= 0 || navigator.userAgent.indexOf("Trident/6")) {
                        el.src = el.src;
                    }
                })(this);
            }
        });
    };
    // Alias functions which often better describe the use case
    jQuery.fn.imageCenterResize = function(callback) {
        return jQuery(this).centerImage('inside', callback);
    };
    jQuery.fn.imageCropFill = function(callback) {
        return jQuery(this).centerImage('outside', callback);
    };
})(jQuery);