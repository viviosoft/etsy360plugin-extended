<?php

class Add_Meta_Box extends Etsy_API
{

    public function __construct()
    {
        add_action('add_meta_boxes', array($this, 'etsy360_add_custom_box'));
    }

    function etsy360_add_custom_box()
    {

        $post_type = array('post', 'page');

        foreach ($post_type as $pt) {
            add_meta_box('etsy360_shortcodes', 'Etsy360 Shortcodes', array( $this, 'side_meta_box' ), $pt, 'side', 'default');
        }

    }

    function side_meta_box()
    {
        $shopSections = Etsy_API::getShopSections();

        echo '<div class="shortcodes">';
        echo '<ul>
            <li><a class="insertShortCode" sortCode="[shop_listings]">Shop Listings</a></li>
         </ul>';
        echo '</div>';

        echo '<h4>Shop Featured Listings</h4>';

        echo '<div class="shortcodes">';
        echo '<ul>';
        echo '<li><a class="insertShortCode" sortCode="[shop_featured_listings]">Shop Featured Listings</a></li>';
        echo '</ul>';
        echo '</div>';


        echo '<h4>Shop Sections</h4>';

        if (empty($shopSections)) {
            echo 'You have no Shop Sections to display here.';
        } else {
            echo '<div class="shortcodes">';
            echo '<ul>';
            foreach ($shopSections as $section) {
                echo '<li><a class="insertShortCode" sortCode="[shop_section_listings  section_id = ' . $section->shop_section_id . ']">' . $section->title . '</a></li>';
            }
            echo '</ul>';
            echo '</div>';
        }
    }

}

$e360_meta_box = new Add_Meta_Box;