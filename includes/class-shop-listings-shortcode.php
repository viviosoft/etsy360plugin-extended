<?php

ob_start();


class Shortcode_Shop_Listings extends Etsy_API
{

    public function __construct()
    {
        add_shortcode('shop_listings', array($this, 'etsy360_add_shop_listings_shortcode'));
    }

    function etsy360_add_shop_listings_shortcode()
    {

        $content = "";

        if ($_GET['item-details']) {

            $itemListingDetails = $this->getListingDetails($_GET['item-details']);

            include_once('etsy360-item-details.php');

            return $content;

        } elseif ($_GET['shop-section']) {

            $itemListings = $this->getShopSectionListings($_GET['shop-section']);

            include_once('etsy360-shop-listing.php');
            return $content;

        } else {

            $itemListings = $this->getShopListings();

            require_once('etsy360-shop-listing.php');
            return $content;

        }


    }

}

$shop_listings_shortcode = new Shortcode_Shop_Listings;