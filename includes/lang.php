<?php

define('PAGING_PREV', __('Previous', 'etsy360'));
define('PAGING_NEXT', __('Next', 'etsy360'));

define('ITEM_QTY_AVAILABLE', __('Qty Available: ', 'etsy360'));
define('ITEM_MATERIALS', __('Materials', 'etsy360'));
define('ITEM_AVAILABLE', __('Available ', 'etsy360'));

define('ITEM_DETAILS_TAB', __('Item Details', 'etsy360'));
define('ITEM_SHIPPING_POLICIES_TAB', __('Shipping and Policies', 'etsy360'));

define('SHIPPING_COST_HEADER', __('Shipping Costs', 'etsy360'));
define('POLICES_HEADER', __('Policies', 'etsy360'));

define('SHIPPING_SHIP_TO', __('Ship To', 'etsy360'));
define('SHIPPING_COST', __('Cost', 'etsy360'));
define('SHIPPING_WITH_ANOTHER_ITEM', __('With another Item', 'etsy360'));

define('POLICES_PAYMENTS', __('Policies Payment', 'etsy360'));
define('POLICES_SHIPPING_INFO', __('Policies Shipping', 'etsy360'));
define('POLICES_REFUNDS', __('Policies Refunds', 'etsy360'));
define('POLICES_ADDITIONAL', __('Additional Info', 'etsy360'));
