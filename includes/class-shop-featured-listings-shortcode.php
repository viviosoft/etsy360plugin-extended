<?php

class Shortcode_Shop_Featured_Listings extends Etsy_API
{

    public function __construct()
    {
        add_shortcode('shop_featured_listings', array($this, 'shop_featured_listings_shortcode'));

    }

    function shop_featured_listings_shortcode($content = null)
    {
        $itemListings = $this->getShopFeatured();
        $truncate_title = (Etsy360_Helper_Class()->get_wp_option('num_char_featured') != '') ? Etsy360_Helper_Class()->get_wp_option('num_char_featured') : 20;
        $selectedPage = is_page(Etsy360_Helper_Class()->get_wp_option('shop_page_selected')) ? "" : Etsy360_Helper_Class()->get_wp_option('shop_page_selected') . "/";

        $featuredListings = true;

        if ($itemListings == false) {
            echo '<div class="alert alert-danger">Sorry, there are no featured shop items to list.  Be sure you have selected featured items in your Etsy Shop.</div>';
            return false;
        }

        if ((!isset($_GET['item-details'])) && (!isset($_GET['shop-section']))) {

            $content .= '<div class="section group">';

            $content .= '<div class="e360-grid e360-grid-pad">';

            foreach ($itemListings->results as $d) {

                $shopCurrency = $this->getCurrencyInfo($d->currency_code);

                $content .= '<div class="e360Col-1-4 mobile-e360Col-1-1">';

                $content .= '<div class="image-wrapper">
                    <a class="item-image-anchor" href="' . $selectedPage . '?item-details=' . $d->listing_id . '&item-name=' . Etsy360_Helper_Class()->clean_item_url_string($d->title) . '" >
                    <img title="' . $d->title . '" alt="' . $d->title . '" class="item-image" src="' . $d->Images[0]->url_170x135 . '" /></a>
                </div>';

                $content .= '<div class="item-details">';

                $content .= '<div class="item-title">';
                $content .= '<a href="' . $selectedPage . '?item-details=' . $d->listing_id . '&item-name=' . Etsy360_Helper_Class()->clean_item_url_string($d->title) . '" style="color: ' . Etsy360_Helper_Class()->get_wp_option('accent_color') . '">' . Etsy360_Helper_Class()->truncate($d->title, $truncate_title) . '</a>';
                $content .= '</div>';

                $content .= '<div class="item-price price-margin-3" style="background-color:' . Etsy360_Helper_Class()->get_wp_option('accent_color') . '">';
                $content .= ($shopCurrency['placement'] == 'before') ? $shopCurrency['symbol'] . $d->price : $d->price . " " . $shopCurrency['symbol'];
                $content .= '</div>';

                $content .= '</div>';

                $content .= '</div>';

            }

            $content .= '</div>';

            $content .= '</div>';
        }

        return $content;

    }

}

$shop_listings_shortcode = new Shortcode_Shop_Featured_Listings;