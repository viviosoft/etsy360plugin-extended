<?php

function load_e360_plugin_scripts()
{
    wp_register_script('jquery-validation-scripts', E360_PLUGIN_URL . 'assets/validation/js/jquery.validate.js', array('jquery'));

//    wp_register_script('miovalidation', E360_PLUGIN_URL . 'assets/validation/js/mioValidation.plugin.js', array('jquery'));
    wp_register_script('general-scripts', E360_PLUGIN_URL . 'assets/js/general-scripts.js', array('jquery'));
    wp_register_script('colorpicker-scripts', E360_PLUGIN_URL . 'assets/color-picker/js/colorPicker.js', array('jquery'));

//    wp_register_style('validation-styles', E360_PLUGIN_URL . 'assets/validation/css/mioValidation.css');
    wp_register_style('icon-set', E360_PLUGIN_URL . 'assets/foundation-icons/foundation-icons.css');
    wp_register_style('plugin-styles', E360_PLUGIN_URL . 'assets/css/etsy360-styles.css', array(), '1.0');
    wp_register_style('colorpicker-styles', E360_PLUGIN_URL . 'assets/color-picker/css/colorPicker.css');

    wp_register_script('fancybox-scripts', E360_PLUGIN_URL . 'assets/fancybox/jquery.fancybox.js');
    wp_register_style('fancybox-styles', E360_PLUGIN_URL . 'assets/fancybox/jquery.fancybox.css');

//    Breadcrumbs
    wp_register_style('breadcrumbs-reset-styles', E360_PLUGIN_URL . 'assets/breadcrumbs/css/reset.css');
    wp_register_style('breadcrumbs-styles', E360_PLUGIN_URL . 'assets/breadcrumbs/css/style.css');
    wp_register_script('breadcrumbs-scripts', E360_PLUGIN_URL . 'assets/breadcrumbs/js/modernizr.js');

    wp_register_style('e360-grid-styles', E360_PLUGIN_URL . 'assets/css/simplegrid.css');

//    wp_enqueue_script('jquery-validation-scripts');
    wp_enqueue_script('miovalidation');
    wp_enqueue_script('general-scripts');
    wp_enqueue_script('colorpicker-scripts');

    wp_enqueue_script('fancybox-scripts');
    wp_enqueue_style('fancybox-styles');

    wp_enqueue_style('e360-grid-styles');

//    Breadcrumbs
    wp_enqueue_script('breadcrumbs-scripts');
    wp_enqueue_style('breadcrumbs-styles');
    wp_enqueue_style('breadcrumbs-reset-styles');


//    wp_enqueue_style('validation-styles');
    wp_enqueue_style('icon-set');
    wp_enqueue_style('plugin-styles');
    wp_enqueue_style('colorpicker-styles');

    if (Etsy360_Helper_Class()->get_wp_option('e_theme') == 'classic' || Etsy360_Helper_Class()->get_wp_option('e_theme') == '') {

        wp_register_style('e360-theme-one', E360_PLUGIN_URL . 'assets/css/e360-theme-classic.css');
        wp_enqueue_style('e360-theme-one');

    } elseif (Etsy360_Helper_Class()->get_wp_option('e_theme') == 'modern') {

        wp_register_style('e360-theme-one', E360_PLUGIN_URL . 'assets/css/e360-theme-modern.css');
        wp_enqueue_style('e360-theme-one');

    }


}

add_action('init', 'load_e360_plugin_scripts');
