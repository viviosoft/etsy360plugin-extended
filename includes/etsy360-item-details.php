<?php

$etsyAPI = new Etsy_API();

//$debug = new PHPDebugger();

$shopPolicies = $etsyAPI->getShopInformation();

$variationsOutput = Etsy360_Helper_Class()->get_variations($itemListingDetails[0]->Variations);

$itemMat = $itemListingDetails[0]->materials;

$shopCurrency = $etsyAPI->getCurrencyInfo($itemListingDetails[0]->currency_code);
$layoutOption = Etsy360_Helper_Class()->get_wp_option('layout_item_page');
$item_title_size = Etsy360_Helper_Class()->get_wp_option('item_title_size');
$item_button_text = Etsy360_Helper_Class()->get_wp_option('item_button_text');

$itemTitleSize = ($item_title_size) ? $item_title_size : 'h2';
$itemButtonText = ($item_button_text) ? $item_button_text : 'ORDER NOW';

if ($layoutOption == 'layout-right' || $layoutOption == null) {
    $alterClass_1_3 = 'class="e360Col-1-2 mobile-e360Col-1-1"';
    $alterClass_2_3 = 'class="e360Col-1-2 mobile-e360Col-1-1"';
} else {
    $alterClass_1_3 = 'style="text-align: center;"';
    $alterListStyle = 'style="list-style: none; margin-left:-30px;"';
}

$price = ($shopCurrency['placement'] == 'before') ? $shopCurrency['symbol'] . $itemListingDetails[0]->price . " " . $itemListingDetails[0]->currency_code : $itemListingDetails[0]->price . " " . $shopCurrency['symbol'] . " " . $itemListingDetails[0]->currency_code;

$buttonPrice = $itemListingDetails[0]->price;

$url = strtok($_SERVER["REQUEST_URI"], '?');

$content .= '

<section>
	<nav>
		<ol class="cd-breadcrumb triangle">
			<li><a href="' . $url . '">Return to Shop</a></li>
			<li class="current"><em style="background-color:' . Etsy360_Helper_Class()->get_wp_option('accent_color') . '; ">' . Etsy360_Helper_Class()->truncate($itemListingDetails[0]->title, 40) . '</em></li>
		</ol>
	</nav>
</section>

';
$content .= '<div id="global-alert" class="cart-alert cart-alert-message message" style=""></div>';

$content .= '<div class="section group">';

$content .= "<" . $itemTitleSize . " style='color: " . Etsy360_Helper_Class()->get_wp_option('accent_color') . "'>" . $itemListingDetails[0]->title . "</" . $itemTitleSize . ">";

$content .= '<div class="e360-grid e360-grid-pad">';

$content .= '<div ' . $alterClass_2_3 . '>';

$content .= '<div id="panel">';

$content .= '<div id="image-wrapper">';

$content .= '<a rel="gallery" class="fancybox" title="' . $itemListingDetails[0]->title . ' - ' . $price . '" href="' . $itemListingDetails[0]->Images[0]->url_fullxfull . '">';

$content .= '<span class="magnifier" style="background-image: url(' . $magIcon . ');"></span>';

$content .= '<img id="largeImage" src="' . $itemListingDetails[0]->Images[0]->url_fullxfull . '" />';
$content .= '</a>';

$content .= '</div>';

$content .= '</div>';


$content .= '<div id="thumbs">';

if(!empty($itemListingDetails[0]->Images)) {

    foreach ($itemListingDetails[0]->Images as $i) {
        $content .= '<img src="' . $i->url_75x75 . '" />';
        //Create a new array of images
        $newAr[] = $i->url_fullxfull;
    }

// We don't need the first key in the array
    unset($newAr[0]);
    foreach ($newAr as $i) {
        $content .= '<a rel="gallery" class="fancybox" style="display:none;" title="' . $itemListingDetails[0]->title . ' - ' . $price . '" href="' . $i . '"><img src="' . $i . '" /></a>';
    }

}

$content .= '</div>';

$content .= '</div>';

$content .= '<div ' . $alterClass_1_3 . '>';

$content .= '<div class="details-container">';

$content .= '<div class="item-price-details" style="color: ' . Etsy360_Helper_Class()->get_wp_option('accent_color') . '">';
$content .= $price;
$content .= '</div>';

$content .= '<div class="category-select">
                <label for="itemQty">' . ITEM_QTY_AVAILABLE . '</label>' .
    Etsy360_Helper_Class()->get_qty($itemListingDetails[0]->quantity) .
    '</div>';

$content .= $variationsOutput;

$content .= '<div class="item-mat-details">';

if (!empty($itemMat)) :
    $content .= '<strong>' . ITEM_MATERIALS . '</strong>';
    $content .= '<ul ' . $alterListStyle . '>';
    foreach ($itemMat as $material) {
        $content .= '<li>' . ucwords($material) . '</li>';
    }
    $content .= '</ul>';
endif;

$content .= '</div>';

$content .= '<a data-modal-id="popup1" target="_blank" data-currency-placement="'.$shopCurrency['placement'].'" data-currency-symbol="'.$shopCurrency['symbol'].'" data-currency-code="'.$itemListingDetails[0]->currency_code.'" data-item-price="' . $buttonPrice . '" data-item-image="' . $itemListingDetails[0]->Images[0]->url_75x75 . '" data-item-name="' . $itemListingDetails[0]->title . '" data-listing-id="' . $itemListingDetails[0]->listing_id . '" href="' . $itemListingDetails[0]->url . '" style="background-color: ' . Etsy360_Helper_Class()->get_wp_option('accent_color') . '" class="add-to-cart shopDetailsButton">' . $itemButtonText . '</a>';

$content .= '</div>';

$content .= '</div>';

$content .= '</div>';


$content .= '<div class="section group">';

$content .= '<div class="etabs">';

$content .= '<ul class="etab-links">';
$content .= '<li class="active"><a href="#tab1">' . ITEM_DETAILS_TAB . '</a></li>';
$content .= '<li><a href="#tab2">' . ITEM_SHIPPING_POLICIES_TAB . '</a></li>';
$content .= '</ul>';

$content .= '<div class="etab-content">';

$content .= '<div id="tab1" class="etab active">';
$content .= '<div>' . preg_replace("/(?<!a href=\")(?<!src=\")((http|ftp)+(s)?:\/\/[^<>\s]+)/i", "<a href=\"\\0\" target=\"blank\">\\0</a>", nl2br($itemListingDetails[0]->description)) . '</div>';
$content .= '</div>'; //!tab1

$content .= '<div id="tab2" class="etab">';

$content .= '<strong>' . SHIPPING_COST_HEADER . '</strong>';
$content .= '<div class="etsy-list-info">';

if (!empty($itemListingDetails[0]->ShippingInfo)) {

    $content .= '<table class="etsy360-table shipping">';

    $content .= '<th>' . SHIPPING_SHIP_TO . '</th>';
    $content .= '<th>' . SHIPPING_COST . '</th>';
    $content .= '<th>' . SHIPPING_WITH_ANOTHER_ITEM . '</th>';

    foreach ($itemListingDetails[0]->ShippingInfo as $shippingDetails) {

        $content .= '<tr>
                                            <td>' . $shippingDetails->destination_country_name . '</td>
                                            <td>' . $shippingDetails->primary_cost . '</td>
                                            <td>' . $shippingDetails->secondary_cost . '</td>
                                         </tr>';

    }

    $content .= '</table>';

}


$content .= '</div>';

$content .= '<strong>' . POLICES_HEADER . '</strong>';
$content .= '<div class="etsy-list-info">';

$content .= '<table class="etsy360-table policies">';
$content .= '<tr><td class="widthFix">' . POLICES_PAYMENTS . '</td><td>' . nl2br($shopPolicies[0]->policy_payment) . '</td></tr>';
$content .= '<tr><td class="widthFix">' . POLICES_SHIPPING_INFO . '</td><td>' . nl2br($shopPolicies[0]->policy_shipping) . '</td></tr>';
$content .= '<tr><td class="widthFix">' . POLICES_REFUNDS . '</td><td>' . nl2br($shopPolicies[0]->policy_refunds) . '</td></tr>';
if ($shopPolicies[0]->policy_additional) {
    $content .= '<tr><td class="spacing">' . POLICES_ADDITIONAL . '</td><td>' . nl2br($shopPolicies[0]->policy_additional) . '</td></tr>';
}
$content .= '</table>';

$content .= '</div>';


$content .= '</div>'; //!tab2

$content .= '</div>'; //!etab-content

$content .= '</div>'; //!etabs

$content .= '</div>'; //!span_1_of_2

$content .= "</div>"; //!section group
