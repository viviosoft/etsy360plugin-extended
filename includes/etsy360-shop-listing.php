<?php

$helper = new Etsy360_Helpers();
$etsyAPI = new Etsy_API();

//$etsyAPI->getSalesTransactions();

$shopInfo = $etsyAPI->getShopInformation();

$items_per_page = (Etsy360_Helper_Class()->get_wp_option('items_per_page') == "" || Etsy360_Helper_Class()->get_wp_option('items_per_page') == null || Etsy360_Helper_Class()->get_wp_option('items_per_page') == 0) ? 20 : Etsy360_Helper_Class()->get_wp_option('items_per_page');

$paginator = new Etsy360_Paging();
$paginator->total = ceil((int)$shopInfo[0]->listing_active_count / 25);
$paginator->itemsPerPage = count($itemListings);
$paginator->range = count($itemListings);

$selectedPage = is_page(Etsy360_Helper_Class()->get_wp_option('shop_page_selected')) ? "" : Etsy360_Helper_Class()->get_wp_option('shop_page_selected') . "/";

if ($shopInfo[0]->listing_active_count > $items_per_page && Etsy360_Helper_Class()->get_wp_option('show_paging_top') == 'true' && !$_GET['shop-section']) {

    $content = '<div class="section group">';
    $content .= $paginator->paginate();
    $content .= '</div>';
}

include('item-listings-content.php');

if ($shopInfo[0]->listing_active_count > $items_per_page && !$_GET['shop-section']) {
    $content .= '<div class="section group">';
    $content .= $paginator->paginate();
    $content .= '</div>';
}



