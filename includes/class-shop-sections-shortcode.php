<?php

class Shortcode_Shop_Sections extends Etsy_API
{

    public function __construct()
    {
        add_shortcode('shop_section_listings', array(&$this, 'etsy360_add_shop_sections_shortcode'));
    }

    function etsy360_add_shop_sections_shortcode($sectionId, $content = null)
    {

        $itemListings = $this->getShopSectionListings($sectionId['section_id'], false);

        if (empty($itemListings)) {
            echo '<div class="alert alert-danger">Sorry, there doesn\'t seem to be any items in this section</div>';
            return false;
        }

        $selectedPage = is_page(Etsy360_Helper_Class()->get_wp_option('shop_page_selected')) ? "" : Etsy360_Helper_Class()->get_wp_option('shop_page_selected') . "/";

        include('item-listings-content.php');

        return $content;

    }

}

$shop_sections_shortcode = new Shortcode_Shop_Sections;