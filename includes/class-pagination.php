<?php

class Etsy360_Paging extends Etsy360_Helpers
{
    public $itemsPerPage;
    public $range;
    public $currentPage;
    public $total;
    public $textNav;
    public $itemSelect;
    private $_navigation;
    private $_link;
    private $_pageNumHtml;
    private $_itemHtml;
    private $linkColor;
    private $page;

    /**
     * Constructor
     */
    public function __construct()
    {
//set default values
        $this->linkColor = '';
        $this->itemsPerPage = 5;
        $this->range = 0;
        $this->currentPage = 1;
        $this->total = 0;
        $this->textNav = true;
        $this->itemSelect = array(5, 25, 50, 100, 'All');
//private values
        $this->_navigation = array(
            'next' => PAGING_NEXT,
            'pre' => PAGING_PREV,
            'ipp' => 'Item per page'
        );
        $this->_link = $this->_getCurrentURL();
        $this->_pageNumHtml = '';
        $this->_itemHtml = '';
        $this->_getPage();
    }

    /**
     * paginate main function
     *
     * @access              public
     * @return              type
     */
    public function paginate()
    {
        //get current page
        if (isset($_GET['current_page'])) {
            $this->currentPage = $_GET['current_page'];
        }
        //get item per page
        if (isset($_GET['item'])) {
            $this->itemsPerPage = $_GET['item'];
        }
        //get page numbers
        return $this->_getPageNumbers();
        //get item per page select box
//        echo $this->_itemHtml = $this->_getItemSelect();
    }

    /**
     * return pagination numbers in a format of UL list
     * @access              public
     * @param               type $parameter
     * @return              string
     */
    public function pageNumbers()
    {
        if (empty($this->_pageNumHtml)) {
            exit('Please call function paginate() first.');
        }
        return $this->_pageNumHtml;
    }

    /**
     * return jump menu in a format of select box
     * @access              public
     * @return              string
     */
    public function itemsPerPage()
    {
        if (empty($this->_itemHtml)) {
            exit('Please call function paginate() first.');
        }
        return $this->_itemHtml;
    }

    /**
     * return page numbers html formats
     *
     * @access              public
     * @return              string
     */
    private function  _getPageNumbers()
    {

        $pageURL = ($_GET['shop-section']) ? $this->_link . '?shop-section=' . $_GET['shop-section'] . '&current_page=' : $this->_link . '?current_page=';

        $paging = '<div class="ePagination ePagination-right">';

        //previous link button
        if ($this->currentPage > 1) {
            $paging .= '<a class="pageNumber" href="' . $pageURL . ($this->currentPage - 1) . '"';
            $paging .= '>' . $this->_navigation['pre'] . '</a>';
        }

        //do ranged pagination only when total pages is greater than the range
        $start = ($this->currentPage <= $this->range) ? 1 : ($this->currentPage - $this->range);
        $end = $this->total;

        //loop through page numbers
        if ($start != '' || $end != '') {
            for ($i = $start; $i <= $end; $i++) {
                $paging .= '<a href="' . $pageURL . $i . '"';
                if ($i == $this->currentPage) {
                    $paging .= ' class="pageNumber active"' . ' style="background:' . $this->get_wp_option('accent_color') . '"';
                } else {
                    $paging .= ' class="pageNumber"';
                }
                $paging .= '>' . $i . '</a>';
            }

        }

        //next link button
        if ($this->currentPage < $end) {
            $paging .= '<a class="pageNumber" href="' . $pageURL . ($this->currentPage + 1) . '"';
            $paging .= '>' . $this->_navigation['next'] . '</a>';
        }
        $paging .= '</div>';

        return $paging;
    }

    /**
     * return item select box
     *
     * @access              public
     * @return              string
     */
    private function  _getItemSelect()
    {
        $items = '';
        $ippArray = $this->itemSelect;
        foreach ($ippArray as $ippOpt) {
            $items .= ($ippOpt == $this->itemsPerPage) ? "<option selected value=\"$ippOpt\">$ippOpt</option>\n" : "<option value=\"$ippOpt\">$ippOpt</option>\n";
        }
        return "<span class=\"paginate\">" . $this->_navigation['ipp'] . "</span>
<select class=\"paginate\" onchange=\"window.location='$this->_link?current=1&item='+this[this.selectedIndex].value;return false\">$items</select>\n";
    }

    private function _getCurrentURL()
    {
        global $wp;
        return strtok($_SERVER["REQUEST_URI"],'?');
//        return substr($_SERVER["HTTP_HOST"], strrpos($_SERVER["REQUEST_URI"], "/") + 1);
    }

    private function _getPage()
    {
        return $this->get_wp_option('shop_page_selected');
    }
//    private function  _getCurrentURL() {
//        $pageURL = 'http';
//        if ($_SERVER["HTTPS"] == "on") {$pageURL .= "s";}
//        $pageURL .= "://";
//        if ($_SERVER["SERVER_PORT"] != "80") {
//            $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
//        } else {
//            $pageURL .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
//        }
//        return $pageURL;
//    }

}
