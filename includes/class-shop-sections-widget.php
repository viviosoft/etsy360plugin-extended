<?php


add_action('widgets_init', create_function('', 'return register_widget("Etsy360_Shop_Section_widget");'));

class Etsy360_Shop_Section_widget extends WP_Widget
{

    /** constructor -- name this the same as the class above */
    function Etsy360_Shop_Section_widget()
    {
        parent::WP_Widget(false, $name = 'Etsy360 Shop Sections');
    }

    /** @see WP_Widget::widget -- do not rename this */
    function widget($args, $instance)
    {

        extract($args);
        $title = apply_filters('widget_title', $instance['title']);
        $page = Etsy360_Helper_Class()->get_wp_option('shop_page_selected');

        $etsy360Data = new Etsy_API();
        $shopSections = $etsy360Data->getShopSections();

        if (empty($shopSections)) {
            echo '<div class="alert alert-warning">You have no Etsy Shop Sections to display.</div>';
            return false;
        }

        ?>
        <?php echo $before_widget; ?>
        <?php

        echo '<h3 class="widget-title">' . $title . '</h3>';

        echo '<div class="shopSectionsSidebar">';
        echo '<ul>';
        foreach ($shopSections as $section) {
            echo '<li><a href="' . site_url() . '/' . $page . '/?shop-section=' . $section->shop_section_id . '">' . ucwords($section->title) . '</a></li>';
        }
        echo '</ul>';
        echo '</div>';

        ?>
        <?php echo $after_widget; ?>
    <?php
    }

    /** @see WP_Widget::update -- do not rename this */
    function update($new_instance, $old_instance)
    {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['page'] = strip_tags($new_instance['page']);
        return $instance;
    }

    /** @see WP_Widget::form -- do not rename this */
    function form($instance)
    {

        $title = esc_attr($instance['title']);
        $page = esc_attr($instance['page']);

        $pages = get_pages();

        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>">Title: </label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>"
                   name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo $title; ?>"/>
        </p>

    <?php
    }


} // end class

?>