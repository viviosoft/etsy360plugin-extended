<?php

function e360_admin_menu()
{
    // Add the top-level admin menu
    $page_title = __('Etsy360');
    $menu_title = __('Etsy360');
    $capability = 'manage_options';
    $menu_slug = 'etsy360-options';
    $function = 'e360_options_page';
    $icon = E360_PLUGIN_URL . 'includes/admin/imgs/etsy360-icon-16x16.png';
    $position = 3;

    add_menu_page($page_title, $menu_title, $capability, $menu_slug, $function, $icon, $position);

}

add_action( 'admin_menu', 'e360_admin_menu');
