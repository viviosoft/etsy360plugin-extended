<?php
/**
 * Admin Options Page
 *
 * @package     Etsy360 Extended
 * @subpackage  Admin Options Page
 * @copyright   Copyright (c) 2014, VivioSoft
 * @since       1.0
 */

//ob_start();


// Exit if accessed directly
if (!defined('ABSPATH')) exit;


/**
 * Retrieve settings tabs
 *
 * @since 2.4
 * @return array $tabs
 */
function e360_get_settings_tabs()
{
    $tabs = array();
    $tabs['general'] = __('General');
    $tabs['styles'] = __('Styles');
    $tabs['licenses'] = __('Licenses');
    $tabs['info'] = __('Info');
    $tabs['extensions'] = __('Extensions');

    return apply_filters('e360_settings_tabs', $tabs);
}

/**
 * Add all settings sections and fields
 *
 * @since 1.0
 * @return void
 */
function e360_register_settings()
{

    if (false == get_option('e360_settings')) {
        add_option('e360_settings');
    }

    foreach (e360_get_registered_settings() as $tab => $settings) {

        add_settings_section(
            'e360_settings_' . $tab,
            __return_null(),
            '__return_false',
            'e360_settings_' . $tab
        );

        foreach ($settings as $option) {

            $name = isset($option['name']) ? $option['name'] : '';

            add_settings_field(
                'e360_settings_[' . $option['id'] . ']',
                $name,
                function_exists('e360_' . $option['type'] . '_callback') ? 'e360_' . $option['type'] . '_callback' : 'e360_missing_callback',
                'e360_settings_' . $tab,
                'e360_settings_' . $tab,
                array(
                    'section' => $tab,
                    'id' => isset($option['id']) ? $option['id'] : null,
                    'desc' => !empty($option['desc']) ? $option['desc'] : '',
                    'name' => isset($option['name']) ? $option['name'] : null,
                    'size' => isset($option['size']) ? $option['size'] : null,
                    'options' => isset($option['options']) ? $option['options'] : '',
                    'std' => isset($option['std']) ? $option['std'] : '',
                    'min' => isset($option['min']) ? $option['min'] : null,
                    'max' => isset($option['max']) ? $option['max'] : null,
                    'step' => isset($option['step']) ? $option['step'] : null,
                    'select2' => isset($option['select2']) ? $option['select2'] : null,
                    'placeholder' => isset($option['placeholder']) ? $option['placeholder'] : null
                )
            );
        }

    }

    // Creates our settings in the options table
    register_setting('e360_settings', 'e360_settings', 'e360_settings_sanitize');

}


/**
 * Retrieve the array of plugin settings
 *
 * @since 2.4
 * @return array
 */
function e360_get_registered_settings()
{

    $e360_settings = array(
        /** General Settings */
        'general' => apply_filters('e360_settings_general',
            array(
                'listings_shop_page' => array(
                    'id' => 'shop_page_selected',
                    'name' => __('Select Shop Page'),
                    'desc' => __('You must first create your shop page. Then select your main shop page here.'),
                    'type' => 'select',
                    'std' => get_option('shop_page_selected'),
                    'options' => e360_get_pages(),
                    'placeholder' => __('Select a page')
                ),
                'shop_language' => array(
                    'id' => 'shop_language',
                    'name' => __('Select Language'),
                    'desc' => __(''),
                    'type' => 'select',
                    'std' => 'en',
                    'options' => array(
                        'en' => 'English',
                        'fr' => 'French',
                        'it' => 'Italian',
                        'de' => 'German',
                        'es' => 'Spanish',
                        'ja' => 'Japanese',
                        'nl' => 'Dutch',
                        'pt' => 'Portuguese',
                        'ru' => 'Russian'
                    ),
                    'placeholder' => __('Select your shop page')
                ),
//                'init_caching' => array(
//                    'id' => 'init_caching',
//                    'name' => __('Shop Caching'),
//                    'type' => 'etsy_caching',
//                ),
                'field_oauth' => array(
                    'id' => 'field_oauth',
                    'name' => '<strong style="font-size: 18px;">' . __('Etsy OAuth Connect') . '</strong>',
                    'desc' => '',
                    'type' => 'header'
                ),
                'api_shop_name' => array(
                    'id' => 'etsy_shop_user',
                    'name' => __('Etsy Shop Name'),
                    'desc' => __('Enter your Etsy Shop Name'),
                    'type' => 'text',
                    'size' => '',
                    'std' => get_option('etsy_shop_user')
                ),
                'connect_oauth' => array(
                    'id' => 'connect_oauth',
                    'type' => 'oauth_api_connect'
                ),
//                'api_key' => array(
//                    'id' => 'etsy_api_key',
//                    'name' => __('Etsy API Key'),
//                    'desc' => __('Enter your Etsy API Key you generated'),
//                    'type' => 'text',
//                    'size' => '',
//                    'std' => get_option('etsy_api_key')
//                ),
                'field_shop_information' => array(
                    'id' => 'field_shop_information',
                    'name' => '<strong style="font-size: 18px;">' . __('Shop Details') . '</strong>',
                    'desc' => '',
                    'type' => 'header'
                ),
                'shop_name' => array(
                    'id' => 'shop_name',
                    'name' => 'Shop Name',
                    'desc' => e360_shop_info_callback()->shop_name,
                    'type' => 'info'
                ),
//                'shop_id' => array(
//                    'id' => 'shop_id',
//                    'name' => 'Shop Id',
//                    'desc' => e360_shop_info_callback()->shop_id,
//                    'type' => 'info'
//                )


            )
        ),
        'licenses' => apply_filters('e360_settings_licenses',
            array()
        ),
        /** Styles Settings */
        'styles' => apply_filters('e360_settings_styles',
            array(
                'field_general_styles' => array(
                    'id' => 'field_general_styles',
                    'name' => '<strong style="font-size: 18px;">' . __('General Style Options') . '</strong>',
                    'desc' => '',
                    'type' => 'header'
                ),
                'general_accent_color' => array(
                    'id' => 'accent_color',
                    'name' => __('Accent Color'),
                    'desc' => __(''),
                    'std' => get_option('accent_color'),
                    'type' => 'color_select'
                ),
                'general_selected_theme' => array(
                    'id' => 'e_theme',
                    'name' => __('Select Theme'),
                    'desc' => __(''),
                    'std' => get_option('e_theme'),
                    'type' => 'select',
                    'options' => array(
                        'classic' => __('Classic Theme'),
                        'modern' => __('Modern Theme')
                    )
                ),
                'field_listing_styles' => array(
                    'id' => 'field_listing_styles',
                    'name' => '<strong style="font-size: 18px;">' . __('Listings Style Options') . '</strong>',
                    'desc' => '',
                    'type' => 'header'
                ),
                'listings_top_paging' => array(
                    'id' => 'show_paging_top',
                    'name' => __('Show Paging on Top'),
                    'desc' => __('Select if you want to show paging numbers on top of your shop page'),
                    'std' => get_option('show_paging_top'),
                    'type' => 'select',
                    'options' => array(
                        'false' => __('No'),
                        'true' => __('Yes')
                    )
                ),
//                'listings_items_per_page' => array(
//                    'id' => 'items_per_page',
//                    'name' => __('Items per page'),
//                    'desc' => __('It is recommend that you don\'t exceed a total of 30 items per page.  It could slow page loads.'),
//                    'type' => 'text',
//                    'size' => 'small',
//                    'std' => get_option('items_per_page')
//                ),
                'listings_select_num_rows' => array(
                    'id' => 'num_rows',
                    'name' => __('Number of Columns'),
                    'desc' => __('Define how many columns you\'d like to have on your page.'),
                    'std' => get_option('num_rows'),
                    'type' => 'select',
                    'options' => array(
                        'e360_span_3' => __('3 Columns'),
                        'e360_span_4' => __('4 Columns'),
                        'e360_span_5' => __('5 Columns'),
                    )
                ),
                'listings_truncate_title' => array(
                    'id' => 'num_char',
                    'name' => __('Truncate Listing Title'),
                    'desc' => __('Select the number of characters you\'d like to see in your main listings titles. Use 0 for no truncating.'),
                    'type' => 'text',
                    'size' => 'small',
                    'std' => get_option('num_char')
                ),
                'featured_listings_truncate_title' => array(
                    'id' => 'num_char_featured',
                    'name' => __('Featured Listing Truncate Title'),
                    'desc' => __('Select the number of characters you\'d like to see in your featured listings titles. Use 0 for no truncating.'),
                    'type' => 'text',
                    'size' => 'small',
                    'std' => get_option('num_char_featured')
                ),
                'field_item_details_options' => array(
                    'id' => 'field_item_details_options',
                    'name' => '<strong style="font-size: 18px;">' . __('Item Details Options') . '</strong>',
                    'desc' => '',
                    'type' => 'header'
                ),
                'details_layout' => array(
                    'id' => 'layout_item_page',
                    'name' => __('select details layout'),
                    'desc' => __(''),
                    'type' => 'details_layout',
                    'std' => get_option('layout_item_page'),
                    'options' => array(
                        'layout-right' => 'page-layout-icon-right.png',
                        'layout-bottom' => 'page-layout-icon-bottom.png'
                    )
                ),
                'details_title_size' => array(
                    'id' => 'item_title_size',
                    'name' => __('Item Title Size'),
                    'desc' => __('Select the size of the titles on the items details page. The large the number, the smaller the size.'),
                    'std' => get_option('item_title_size'),
                    'type' => 'select',
                    'options' => array(
                        'h1' => __('Heading 1'),
                        'h2' => __('Heading 2'),
                        'h3' => __('Heading 3'),
                        'h4' => __('Heading 4'),
                        'h5' => __('Heading 5'),
                        'h6' => __('Heading 6'),
                    )
                ),
                'details_button_text' => array(
                    'id' => 'item_button_text',
                    'name' => __('Item Button Text'),
                    'desc' => __(''),
                    'type' => 'text',
                    'size' => '',
                    'std' => get_option('item_button_text')
                )
            )
        ),
        'info' => apply_filters('e360_setting_info',
            array(
                'system_info' => array(
                    'id' => 'system_info',
                    'type' => 'system_info',
                    'name' => __('System Information')
                ),
                'field_debug' => array(
                    'id' => 'field_debug',
                    'name' => '<strong style="font-size: 18px;">' . __('Debugging') . '</strong>',
                    'desc' => '',
                    'type' => 'header'
                ),
                'debugging' => array(
                    'id' => 'debugging',
                    'name' => __('Debugging'),
                    'desc' => __('Used to help with debug plugin'),
                    'std' => get_option('debugging'),
                    'type' => 'checkbox'
                )
            )

        ),
        'extensions' => apply_filters('e360_extensions_info',
            array(
                'system_extensions' => array(
                    'id' => 'extensions',
                    'type' => 'extensions',
                    'name' => __('Extensions')
                )
            )

        )

    ); //!end settings array

    return apply_filters('e360_registered_settings', $e360_settings);

}

add_action('admin_init', 'e360_register_settings');


function e360_etsy_caching_callback($args)
{

    $date_last_cached = get_option('cached_on_date');


    global $wpdb;

    $table_name = $wpdb->prefix . 'etsy_cached_shop';
    $db_results = $wpdb->get_results("SELECT * FROM $table_name ");


    if (strtotime('-1 seconds') > $date_last_cached && !empty($db_results)) {

        $date_last_cached_formatted = date('m/d/Y @ H:i a', $date_last_cached);
        $html = '<div class="infoMessage"><p>';
        $html .= __("Caching was last performed on: $date_last_cached_formatted");
        $html .= "</p></div>";

    } else {

        $html = '<div class="admin_error"><p>';
        $html .= __("Need to run Caching");
        $html .= "</p></div>";

    }

    $html .= '<input type="submit" id="cache-btn" class="button button-primary" name="' . $args['id'] . '" value="' . __('Cache Shop') . '"/>';
    $html .= '<span id="loading-container" style="display: none;"><img style="vertical-align: middle;width: 23px; margin: 0 12px;" src="' . plugin_dir_url(__FILE__) . 'imgs/ajax-loader.gif">Please wait, caching in progress... this could take several minutes to complete.</span>';

    echo $html;
}

function e360_extensions_callback()
{

    $html = '
        <div class="e360-extension">
            <h3 class="e360-extension-title">Cart Extension</h3>
            <a target="_blank" href="https://www.etsy360.com/downloads/etsy360-cart-extension/?ref=2" title="Cart Extension">
                <img width="320" src="https://www.etsy360.com/wp-content/uploads/edd/2015/01/etsy-cart-extension-wordpress-etsy360.png" class="" alt="edd-conditional-emails" title="Cart Extension">
            </a>
            <p></p>
            <p>Allowing your visitors to add items to a shopping cart</p>
            <p></p>
            <a target="_blank" href="https://www.etsy360.com/downloads/etsy360-cart-extension/?ref=2" title="Conditional Emails" class="button-primary">Get this Add On</a>
        </div>';

    echo $html;
}

//function e360_api_directions_callback($args)
//{
//    global $e360_options;
//
////    $shopInfo = e360_shop_info_callback();
//
////    $debug = new PHPDebugger();
////    $debug->debugArr(get_option('e360_settings'));
//
//    if (!isset($shopInfo)) {
//
//        $html = '<table>
//            <tr>
//                <td>
//                    <h3>Etsy API Connection Directions:</h3>
//                    <ol>
//                        <li><strong>Go Here:</strong> <a target="_blank"
//                                                         href="https://www.etsy.com/developers/register">https://www.etsy.com/developers/register</a>
//                        </li>
//                        <li><strong>You may be asked to log into your Etsy account. Log in and enter the
//                                following
//                                information asked on the <strong>Create New App</strong> screen.</li>
//                        <li><strong>Application Name:</strong> My Shop Listings</li>
//                        <li><strong>Describe your Application:</strong> Used to display my Etsy Listings on
//                            my
//                            WordPress
//                            website
//                        </li>
//                        <li><strong>Application Website:</strong> http://www.yourwebiste.com</li>
//                        <li><strong>What type of application are you building?:</strong> Check Seller Tools
//                        </li>
//                        <li><strong>Who will be the users of this application?:</strong> Check The general
//                            public
//                        </li>
//                        <li><strong>Will your app do any of the following?:</strong> Check Read sales data
//                        </li>
//                        <li>Click Read Terms and Create App</li>
//                        <li>Copy the KEYSTRING and paste it into the API Key below</li>
//                    </ol>
//                </td>
//            </tr>
//        </table>';
//
//    } else {
//        $html = '<h4>You\'re API information is correct.</h4>';
//    }
//
//    echo $html;
//
//}

function e360_shop_info_callback()
{

    global $wpdb;

    $etsy360Data = new Etsy_API();

    $shopInfo = $etsy360Data->getShopInformation();
    return $shopInfo[0];

}

function e360_system_info_callback()
{

    $helpers = new Etsy360_Helpers();

    $theme = wp_get_theme();
    $browser = $helpers->get_browser();
    $plugins = $helpers->get_all_plugins();
    $active_plugins = $helpers->get_active_plugins();
    $memory_limit = ini_get('memory_limit');
    $memory_usage = $helpers->get_memory_usage();
    $all_options = $helpers->get_all_options();
    $all_options_serialized = serialize($all_options);
    $all_options_bytes = round(mb_strlen($all_options_serialized, '8bit') / 1024, 2);
    $all_options_transients = $helpers->get_transients_in_options($all_options);

    ?>
    <textarea readonly="readonly" wrap="off" style="width: 100%; height: 300px">
        WordPress Version: <?php echo get_bloginfo('version') . "\n"; ?>
        PHP Version: <?php echo PHP_VERSION . "\n"; ?>
        Web Server: <?php echo $_SERVER['SERVER_SOFTWARE'] . "\n"; ?>

        WordPress URL: <?php echo get_bloginfo('wpurl') . "\n"; ?>
        Home URL: <?php echo get_bloginfo('url') . "\n"; ?>

        Content Directory: <?php echo WP_CONTENT_DIR . "\n"; ?>
        Content URL: <?php echo WP_CONTENT_URL . "\n"; ?>
        Plugins Directory: <?php echo WP_PLUGIN_DIR . "\n"; ?>
        Plugins URL: <?php echo WP_PLUGIN_URL . "\n"; ?>
        Uploads
        Directory: <?php echo (defined('UPLOADS') ? UPLOADS : WP_CONTENT_DIR . '/uploads') . "\n"; ?>

        Cookie
        Domain: <?php echo defined('COOKIE_DOMAIN') ? COOKIE_DOMAIN ? COOKIE_DOMAIN . "\n" : _e('Disabled', 'sysinfo') . "\n" : _e('Not set', 'sysinfo') . "\n" ?>
        Multi-Site
        Active: <?php echo is_multisite() ? _e('Yes', 'sysinfo') . "\n" : _e('No', 'sysinfo') . "\n" ?>

        PHP cURL
        Support: <?php echo (function_exists('curl_init')) ? _e('Yes', 'sysinfo') . "\n" : _e('No', 'sysinfo') . "\n"; ?>

        PHP GD
        Support: <?php echo (function_exists('gd_info')) ? _e('Yes', 'sysinfo') . "\n" : _e('No', 'sysinfo') . "\n"; ?>
        PHP Memory Limit: <?php echo $memory_limit . "\n"; ?>
        PHP Memory

        Usage: <?php echo $memory_usage . "M (" . round($memory_usage / $memory_limit * 100, 0) . "%)\n"; ?>
        PHP Post Max Size: <?php echo ini_get('post_max_size') . "\n"; ?>
        PHP Upload Max Size: <?php echo ini_get('upload_max_filesize') . "\n"; ?>

        WP Options Count: <?php echo count($all_options) . "\n"; ?>
        WP Options Size: <?php echo $all_options_bytes . "kb\n" ?>
        WP Options Transients: <?php echo count($all_options_transients) . "\n"; ?>

        WP_DEBUG: <?php echo defined('WP_DEBUG') ? WP_DEBUG ? _e('Enabled', 'sysinfo') . "\n" : _e('Disabled', 'sysinfo') . "\n" : _e('Not set', 'sysinfo') . "\n" ?>
        SCRIPT_DEBUG: <?php echo defined('SCRIPT_DEBUG') ? SCRIPT_DEBUG ? _e('Enabled', 'sysinfo') . "\n" : _e('Disabled', 'sysinfo') . "\n" : _e('Not set', 'sysinfo') . "\n" ?>
        SAVEQUERIES: <?php echo defined('SAVEQUERIES') ? SAVEQUERIES ? _e('Enabled', 'sysinfo') . "\n" : _e('Disabled', 'sysinfo') . "\n" : _e('Not set', 'sysinfo') . "\n" ?>
        AUTOSAVE_INTERVAL: <?php echo defined('AUTOSAVE_INTERVAL') ? AUTOSAVE_INTERVAL ? AUTOSAVE_INTERVAL . "\n" : _e('Disabled', 'sysinfo') . "\n" : _e('Not set', 'sysinfo') . "\n" ?>
        WP_POST_REVISIONS: <?php echo defined('WP_POST_REVISIONS') ? WP_POST_REVISIONS ? WP_POST_REVISIONS . "\n" : _e('Disabled', 'sysinfo') . "\n" : _e('Not set', 'sysinfo') . "\n" ?>

        Operating System: <?php echo $browser['platform'] . "\n"; ?>
        Browser: <?php echo $browser['name'] . ' ' . $browser['version'] . "\n"; ?>
        User Agent: <?php echo $browser['user_agent'] . "\n"; ?>

        Active Theme:
        - <?php echo $theme->get('Name') ?> <?php echo $theme->get('Version') . "\n"; ?>
        <?php echo $theme->get('ThemeURI') . "\n"; ?>

        Active Plugins:
        <?php

        foreach ($plugins as $plugin_path => $plugin) {
            // Only show active plugins
            if (in_array($plugin_path, $active_plugins)) {
                echo '- ' . $plugin['Name'] . ' ' . $plugin['Version'] . "\n";

                if (isset($plugin['PluginURI'])) {
                    echo '  ' . $plugin['PluginURI'] . "\n";
                }

                echo "\n";
            }
        }
        ?>
    </textarea>
    <?php
}

/**
 * Checkbox Callback
 *
 * Renders checkboxes.
 *
 * @since 1.0
 * @param array $args Arguments passed by the setting
 * @global $edd_options Array of all the EDD Options
 * @return void
 */
function e360_checkbox_callback($args)
{
    global $e360_options;

    $checked = isset($e360_options[$args['id']]) ? checked(1, $e360_options[$args['id']], false) : '';
    $html = '<input type="checkbox" id="e360_settings[' . $args['id'] . ']" name="e360_settings[' . $args['id'] . ']" value="1" ' . $checked . '/>';
    $html .= '<label for="e360_settings[' . $args['id'] . ']"> ' . $args['desc'] . '</label>';

    echo $html;
}


/**
 * Registers the license field callback for Software Licensing
 *
 * @since 1.5
 * @param array $args Arguments passed by the setting
 * @global $edd_options Array of all the EDD Options
 * @return void
 */
if (!function_exists('e360_license_key_callback')) {
    function e360_license_key_callback($args)
    {
        global $e360_options;

        if (isset($e360_options[$args['id']]))
            $value = $e360_options[$args['id']];
        else
            $value = isset($args['std']) ? $args['std'] : '';

        $size = (isset($args['size']) && !is_null($args['size'])) ? $args['size'] : 'regular';

        $html = '<input type="text" class="' . $size . '-text" id="e360_settings[' . $args['id'] . ']" name="e360_settings[' . $args['id'] . ']" value="' . esc_attr($value) . '"/>';

        if ('valid' == get_option($args['options']['is_valid_license_option'])) {
            $html .= '<img style="vertical-align: middle;width: 23px;padding-bottom: 5px; margin: 0 2px;" src="' . plugin_dir_url(__FILE__) . '/imgs/check-mark.png">';
            $html .= '<input type="submit" class="button-secondary" name="' . $args['id'] . '_deactivate" value="' . __('Deactivate License') . '"/>';
        }

        $html .= '<label for="e360_settings[' . $args['id'] . ']"> ' . $args['desc'] . '</label>';

        echo $html;
    }
}


function e360_color_select_callback($args)
{

    global $e360_options;

    if (isset($e360_options[$args['id']]))
        $value = $e360_options[$args['id']];
    else
        $value = isset($args['std']) ? $args['std'] : '';

    $html = '<a href="#" style="background-color: ' . esc_attr($value) . '" class="pickButton getCode">Select Color</a>';
    $html .= '<input type="text" name="e360_settings[' . $args['id'] . ']" class="colorInput" id="colorCode" style="border-bottom: 3px solid ' . esc_attr($value) . ' " value="' . esc_attr($value) . '"/>';

    echo $html;
}

/**
 * Text Callback
 *
 * Renders text fields.
 *
 * @since 1.0
 * @param array $args Arguments passed by the setting
 * @global $edd_options Array of all the EDD Options
 * @return void
 */
function e360_text_callback($args)
{
    global $e360_options;

    if (isset($e360_options[$args['id']]))
        $value = $e360_options[$args['id']];
    else
        $value = isset($args['std']) ? $args['std'] : '';

    $size = (isset($args['size']) && !is_null($args['size'])) ? $args['size'] : 'regular';
    $html = '<input type="text" class="' . $size . '-text" id="e360_settings[' . $args['id'] . ']" name="e360_settings[' . $args['id'] . ']" value="' . esc_attr(stripslashes($value)) . '"/>';
    $html .= '<label for="e360_settings[' . $args['id'] . ']"> ' . $args['desc'] . '</label>';

    echo $html;
}


/**
 * Get Settings
 *
 * Retrieves all plugin settings
 *
 * @since 1.0
 * @return array EDD settings
 */
function e360_get_settings()
{

    $settings = get_option('e360_settings');

    if (empty($settings)) {

        // Update old settings with new single option

        $general_settings = is_array(get_option('e360_settings_general')) ? get_option('e360_settings_general') : array();
        $style_settings = is_array(get_option('e360_settings_styles')) ? get_option('e360_settings_styles') : array();
        $license_settings = is_array(get_option('e360_settings_licenses')) ? get_option('e360_settings_licenses') : array();

        $settings = array_merge($general_settings, $style_settings, $license_settings);

        update_option('e360_settings', $settings);

    }

    return apply_filters('e360_get_settings', $settings);
}

/**
 * Retrieve a list of all published pages
 *
 * On large sites this can be expensive, so only load if on the settings page or $force is set to true
 *
 * @since 2.4
 * @param bool $force Force the pages to be loaded even if not on settings
 * @return array $pages_options An array of the pages
 */
function e360_get_pages($force = false)
{

    $pages_options = array('' => ''); // Blank option

    if ((!isset($_GET['page']) || 'etsy360-options' != $_GET['page']) && !$force) {
        return $pages_options;
    }

    $pages = get_pages();
    if ($pages) {
        foreach ($pages as $page) {
            $pages_options[$page->post_name] = $page->post_title;
        }
    }

    return $pages_options;
}

function e360_details_layout_callback($args)
{
    global $e360_options;

    $html = '<table>';
    $html .= '<tr>';

    foreach ($args['options'] as $key => $option) :
        $checked = false;

        if (isset($e360_options[$args['id']]) && $e360_options[$args['id']] == $key)
            $checked = true;
        elseif (isset($args['std']) && $args['std'] == $key && !isset($e360_options[$args['id']]))
            $checked = true;

        $html .= '<td><input name="e360_settings[' . $args['id'] . ']" type="radio" value="' . $key . '" ' . checked(true, $checked, false) . '/><br/>
                    <img src="' . E360_PLUGIN_URL . 'includes/admin/imgs/' . $option . '">
                </td>';

    endforeach;

    $html .= '</tr>';
    $html .= '</table>';


    echo $html;

}

/**
 * Radio Callback
 *
 * Renders radio boxes.
 *
 * @since 1.3.3
 * @param array $args Arguments passed by the setting
 * @global $edd_options Array of all the EDD Options
 * @return void
 */
function e360_radio_callback($args)
{
    global $e360_options;

    foreach ($args['options'] as $key => $option) :
        $checked = false;

        if (isset($e360_options[$args['id']]) && $e360_options[$args['id']] == $key)
            $checked = true;
        elseif (isset($args['std']) && $args['std'] == $key && !isset($edd_options[$args['id']]))
            $checked = true;

        echo '<input name="e360_settings[' . $args['id'] . ']"" id="e360_settings[' . $args['id'] . '][' . $key . ']" type="radio" value="' . $key . '" ' . checked(true, $checked, false) . '/>';
        echo '<label for="e360_settings[' . $args['id'] . '][' . $key . ']">' . $option . '</label><br/>';
    endforeach;

    echo '<p class="description">' . $args['desc'] . '</p>';
}


/**
 * Select Callback
 *
 * Renders select fields.
 *
 * @since 1.0
 * @param array $args Arguments passed by the setting
 * @global $edd_options Array of all the EDD Options
 * @return void
 */
function e360_select_callback($args)
{
    global $e360_options;

//    $debug = new PHPDebugger();
//    $debug->debugArr($args);

    if (isset($e360_options[$args['id']]))
        $value = $e360_options[$args['id']];
    else
        $value = isset($args['std']) ? $args['std'] : '';

    if (isset($args['placeholder']))
        $placeholder = $args['placeholder'];
    else
        $placeholder = '';

    $html = '<select id="e360_settings[' . $args['id'] . ']" name="e360_settings[' . $args['id'] . ']" ' . ($args['select2'] ? 'class="edd-select2"' : '') . 'data-placeholder="' . $placeholder . '" />';

    foreach ($args['options'] as $option => $name) :
        $selected = selected($option, $value, false);
        $html .= '<option value="' . $option . '" ' . $selected . '>' . $name . '</option>';
    endforeach;

    $html .= '</select>';
    $html .= '<label for="e360_settings[' . $args['id'] . ']"> ' . $args['desc'] . '</label>';

    echo $html;
}

/**
 * Missing Callback
 *
 * If a function is missing for settings callbacks alert the user.
 *
 * @since 1.3.1
 * @param array $args Arguments passed by the setting
 * @return void
 */
function e360_missing_callback($args)
{
    printf(__('The callback function used for the <strong>%s</strong> setting is missing.', 'edd'), $args['id']);
}

/**
 * Header Callback
 *
 * Renders the header.
 *
 * @since 1.0
 * @param array $args Arguments passed by the setting
 * @return void
 */
function e360_header_callback($args)
{
    echo '<hr/>';
}

function e360_info_callback($args)
{
    echo $args['desc'];
}

/**
 * Settings Sanitization
 *
 * Adds a settings error (for the updated message)
 * At some point this will validate input
 *
 * @since 1.0.8.2
 *
 * @param array $input The value inputted in the field
 *
 * @return string $input Sanitizied value
 */
function e360_settings_sanitize($input = array())
{

    global $e360_options;

    if (empty($_POST['_wp_http_referer'])) {
        return $input;
    }

    parse_str($_POST['_wp_http_referer'], $referrer);

    $settings = e360_get_registered_settings();
    $tab = isset($referrer['tab']) ? $referrer['tab'] : 'general';

    $input = $input ? $input : array();
    $input = apply_filters('e360_settings_' . $tab . '_sanitize', $input);

    // Loop through each setting being saved and pass it through a sanitization filter
    foreach ($input as $key => $value) {

        // Get the setting type (checkbox, select, etc)
        $type = isset($settings[$tab][$key]['type']) ? $settings[$tab][$key]['type'] : false;

        if ($type) {
            // Field type specific filter
            $input[$key] = apply_filters('e360_settings_sanitize_' . $type, $value, $key);
        }

        // General filter
        $input[$key] = apply_filters('e360_settings_sanitize', $input[$key], $key);
    }

    // Loop through the whitelist and unset any that are empty for the tab being saved
    if (!empty($settings[$tab])) {
        foreach ($settings[$tab] as $key => $value) {

            // settings used to have numeric keys, now they have keys that match the option ID. This ensures both methods work
            if (is_numeric($key)) {
                $key = $value['id'];
            }

            if (empty($input[$key])) {
                unset($e360_options[$key]);
            }

        }
    }

    // Merge our new settings with the existing
    $output = array_merge($e360_options, $input);

//    $debug = new PHPDebugger();
//    $debug->debugArr($output);
//    exit;

    add_settings_error('e360-notices', '', __('Settings updated.'), 'updated');

    return $output;
}

function e360_connect_api_oauth()
{

    if ($_GET['oauth_token']) {

        // Need to call this operation again to store the new access token and secret into db.
        if ($success = oauth_etsy_class()->Initialize()) {
            if ($success = oauth_etsy_class()->Process()) {
            }
        }

        $url = site_url() . '/wp-admin/admin.php?page=etsy360-options&tab=api&oauth_success=true';
        wp_redirect($url);

    }

    if ($_GET['oauth_success']) {

        if ($success = oauth_etsy_class()->Initialize()) {
            if ($success = oauth_etsy_class()->Process()) {
            }
        }
    }

    // Called when connect button is clicked
    if (isset($_POST['connect_oauth'])) {

        if ($success = oauth_etsy_class()->Initialize()) {
            if ($success = oauth_etsy_class()->Process()) {
            }
        }
        die();

    }

    if (isset($_POST['reset_oauth'])) {

        global $wpdb;
        $table_name = $wpdb->prefix . 'etsy_oauth';

        $wpdb->query('TRUNCATE TABLE ' . $table_name . '');

        oauth_etsy_class()->ResetAccessToken();
        unset($_SESSION['OAUTH_ACCESS_TOKEN']);

        session_destroy();

    }

}

add_action('admin_init', 'e360_connect_api_oauth', 1);

function e360_oauth_api_connect_callback($args)
{
    global $wpdb;
    $table_name = $wpdb->prefix . 'etsy_oauth';
    $db_results = $wpdb->get_row("SELECT id, session, state, access_token, access_token_secret, expiry, authorized, type, server, creation, user FROM " . $table_name . " WHERE user = '1'");

    if (!empty($db_results)) {

        $html = '<img style="vertical-align: middle;width: 23px;padding-bottom: 5px; margin: 0 2px;" src="' . plugin_dir_url(__FILE__) . 'imgs/check-mark.png"> OAuth Connected! ';
        $html .= '<input type="submit" class="button-secondary" id="reset_oauth" name="reset_oauth" value="' . __('Reset') . '"/>';

    } else {

//        $e360_option = get_option('e360_settings');
//        if (!empty($e360_option['api_secret']) && !empty($e360_option['etsy_api_key'])) {

        $html = '<input type="submit" class="button-secondary" name="' . $args['id'] . '" value="' . __('Connect') . '"/>';
//        }
    }


    echo $html;
}


?>