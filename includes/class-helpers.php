<?php

class Etsy360_Helpers
{
    private static $instance;

    public static function instance()
    {
        if (!isset(self::$instance)) {
            self::$instance = new Etsy360_Helpers;
        }
        return self::$instance;
    }

    public function get_qty($qty)
    {

        if (!class_exists('Etsy360Cart')) {

            $qtyOutput = '';
            $qtyOutput .= $qty;
            return $qtyOutput;

        } else {

            $qtyOutput = '<select name="itemQty" id="itemQty">';

            for ($x = 1; $x <= $qty; $x++) {

                $qtyOutput .= '<option value="' . $x . '">' . $x . '</option>';

                $qtyOutput .= $x;

            }

            $qtyOutput .= '</select>';

            return $qtyOutput;

        }


    }

    public function get_variations($variations)
    {

        $variationOutput = '';

        if (!empty($variations)) {

            if (!class_exists('Etsy360Cart')) {

                foreach ($variations as $variation) :

                    $variationOutput .= '<div class="category-select">';

                    $variationOutput .= '<strong>' . ITEM_AVAILABLE . ' ' . $variation->formatted_name . '</strong>';

                    $variationOutput .= '<ul>';

                    foreach ($variation->options as $option) :
                        $variationOutput .= '<li>' . $option->value . ' (' . $option->price . ')' . '</li>';
                    endforeach;
                    $variationOutput .= '</ul>';

                    $variationOutput .= '</div>';

                endforeach;

                return $variationOutput;

            } else {

                foreach ($variations as $variation) :

                    $variationOutput .= '<div class="category-select">';

                    $variationOutput .= '<label for="variations' . $variation->property_id . '">' . ITEM_AVAILABLE . ' ' . $variation->formatted_name . ': </label>';

                    $variationOutput .= '<select name="variation_' . $variation->formatted_name . '" id="variations' . $variation->property_id . '" class="variations">';

                    $variationOutput .= '<option data-variation-property="' . $variation->property_id . '" value="0">Select ' . $variation->formatted_name . '</option>';

                    foreach ($variation->options as $option) :

//                        if($option->price_diff) {
                            $variationOutput .= '<option data-price-diff="'.$option->price_diff.'" data-variation-name="'.$option->value.'" data-variation-price="' . number_format($option->price, 2) . '" data-variation-property="' . $variation->property_id . '" value="' . $option->value_id . '">' . $option->value . ' [' . number_format($option->price, 2) . ']' . '</option>';
//                        } else {
//                            $variationOutput .= '<option data-price-diff="'.$option->price_diff.'" data-variation-name="'.$option->value.'" data-variation-price="' . number_format($option->price, 2) . '" data-variation-property="' . $variation->property_id . '" value="' . $option->value_id . '">' . $option->value . ' ' . '</option>';
//                        }

                    endforeach;
                    $variationOutput .= '</select>';

                    $variationOutput .= '</div>';

                endforeach;

                return $variationOutput;

            }

        } else {
            return $variationOutput;
        }

    }

    public function get_wp_option($option_name)
    {
        $e360_option = get_option('e360_settings');
        // See if we have the NEW option in the database
        if ($e360_option[$option_name]) {
            $option = $e360_option[$option_name];
        } else {
            // If not then just use the original option
            $option = get_option($option_name);
        }
        return $option;
    }

    function clean_item_url_string($str)
    {
        $pattern = '/[^a-zA-Z0-9]/';
        $replace = '-';
        return preg_replace($pattern, $replace, $str);
    }

    function get_include_contents($filename)
    {
        if (is_file($filename)) {
            ob_start();
            include $filename;
            return ob_get_clean();
        }
        return false;
    }

    function get_browser()
    {
        // http://www.php.net/manual/en/function.get-browser.php#101125.
        // Cleaned up a bit, but overall it's the same.

        $user_agent = $_SERVER['HTTP_USER_AGENT'];
        $browser_name = 'Unknown';
        $platform = 'Unknown';
        $version = "";

        // First get the platform
        if (preg_match('/linux/i', $user_agent)) {
            $platform = 'Linux';
        } elseif (preg_match('/macintosh|mac os x/i', $user_agent)) {
            $platform = 'Mac';
        } elseif (preg_match('/windows|win32/i', $user_agent)) {
            $platform = 'Windows';
        }

        // Next get the name of the user agent yes seperately and for good reason
        if (preg_match('/MSIE/i', $user_agent) && !preg_match('/Opera/i', $user_agent)) {
            $browser_name = 'Internet Explorer';
            $browser_name_short = "MSIE";
        } elseif (preg_match('/Firefox/i', $user_agent)) {
            $browser_name = 'Mozilla Firefox';
            $browser_name_short = "Firefox";
        } elseif (preg_match('/Chrome/i', $user_agent)) {
            $browser_name = 'Google Chrome';
            $browser_name_short = "Chrome";
        } elseif (preg_match('/Safari/i', $user_agent)) {
            $browser_name = 'Apple Safari';
            $browser_name_short = "Safari";
        } elseif (preg_match('/Opera/i', $user_agent)) {
            $browser_name = 'Opera';
            $browser_name_short = "Opera";
        } elseif (preg_match('/Netscape/i', $user_agent)) {
            $browser_name = 'Netscape';
            $browser_name_short = "Netscape";
        }

        // Finally get the correct version number
        $known = array('Version', $browser_name_short, 'other');
        $pattern = '#(?<browser>' . join('|', $known) . ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
        if (!preg_match_all($pattern, $user_agent, $matches)) {
            // We have no matching number just continue
        }

        // See how many we have
        $i = count($matches['browser']);
        if ($i != 1) {
            // We will have two since we are not using 'other' argument yet
            // See if version is before or after the name
            if (strripos($user_agent, "Version") < strripos($user_agent, $browser_name_short)) {
                $version = $matches['version'][0];
            } else {
                $version = $matches['version'][1];
            }
        } else {
            $version = $matches['version'][0];
        }

        // Check if we have a number
        if ($version == null || $version == "") {
            $version = "?";
        }

        return array(
            'user_agent' => $user_agent,
            'name' => $browser_name,
            'version' => $version,
            'platform' => $platform,
            'pattern' => $pattern
        );
    }

    function get_all_plugins()
    {
        return get_plugins();
    }

    function get_active_plugins()
    {
        return get_option('active_plugins', array());
    }

    function get_memory_usage()
    {
        return round(memory_get_usage() / 1024 / 1024, 2);
    }

    function get_all_options()
    {
        // Not to be confused with the core deprecated get_alloptions

        return wp_load_alloptions();
    }

    function get_transients_in_options($options)
    {
        $transients = array();

        foreach ($options as $name => $value) {
            if (stristr($name, 'transient')) {
                $transients[$name] = $value;
            }
        }

        return $transients;
    }


    /**
     * Parse the plugin contents to retrieve plugin's metadata.
     *
     * The metadata of the plugin's data searches for the following in the plugin's
     * header. All plugin data must be on its own line. For plugin description, it
     * must not have any newlines or only parts of the description will be displayed
     * and the same goes for the plugin data. The below is formatted for printing.
     *
     * <code>
     * /*
     * Plugin Name: Name of Plugin
     * Plugin URI: Link to plugin information
     * Description: Plugin Description
     * Author: Plugin author's name
     * Author URI: Link to the author's web site
     * Version: Must be set in the plugin for WordPress 2.3+
     * Text Domain: Optional. Unique identifier, should be same as the one used in
     *        plugin_text_domain()
     * Domain Path: Optional. Only useful if the translations are located in a
     *        folder above the plugin's base path. For example, if .mo files are
     *        located in the locale folder then Domain Path will be "/locale/" and
     *        must have the first slash. Defaults to the base folder the plugin is
     *        located in.
     * Network: Optional. Specify "Network: true" to require that a plugin is activated
     *        across all sites in an installation. This will prevent a plugin from being
     *        activated on a single site when Multisite is enabled.
     *  * / # Remove the space to close comment
     * </code>
     *
     * Plugin data returned array contains the following:
     *        'Name' - Name of the plugin, must be unique.
     *        'Title' - Title of the plugin and the link to the plugin's web site.
     *        'Description' - Description of what the plugin does and/or notes
     *        from the author.
     *        'Author' - The author's name
     *        'AuthorURI' - The authors web site address.
     *        'Version' - The plugin version number.
     *        'PluginURI' - Plugin web site address.
     *        'TextDomain' - Plugin's text domain for localization.
     *        'DomainPath' - Plugin's relative directory path to .mo files.
     *        'Network' - Boolean. Whether the plugin can only be activated network wide.
     *
     * Some users have issues with opening large files and manipulating the contents
     * for want is usually the first 1kiB or 2kiB. This function stops pulling in
     * the plugin contents when it has all of the required plugin data.
     *
     * The first 8kiB of the file will be pulled in and if the plugin data is not
     * within that first 8kiB, then the plugin author should correct their plugin
     * and move the plugin data headers to the top.
     *
     * The plugin file is assumed to have permissions to allow for scripts to read
     * the file. This is not checked however and the file is only opened for
     * reading.
     *
     * @link http://trac.wordpress.org/ticket/5651 Previous Optimizations.
     * @link http://trac.wordpress.org/ticket/7372 Further and better Optimizations.
     * @since 1.5.0
     *
     * @param string $plugin_file Path to the plugin file
     * @param bool $markup Optional. If the returned data should have HTML markup applied. Defaults to true.
     * @param bool $translate Optional. If the returned data should be translated. Defaults to true.
     * @return array See above for description.
     */
    function get_plugin_data($plugin_file, $markup = true, $translate = true)
    {

        $default_headers = array(
            'Name' => 'Plugin Name',
            'PluginURI' => 'Plugin URI',
            'Version' => 'Version',
            'Description' => 'Description',
            'Author' => 'Author',
            'AuthorURI' => 'Author URI',
            'TextDomain' => 'Text Domain',
            'DomainPath' => 'Domain Path',
            'Network' => 'Network',
            // Site Wide Only is deprecated in favor of Network.
            '_sitewide' => 'Site Wide Only',
        );

        $plugin_data = get_file_data($plugin_file, $default_headers, 'plugin');

        // Site Wide Only is the old header for Network
        if (!$plugin_data['Network'] && $plugin_data['_sitewide']) {
            _deprecated_argument(__FUNCTION__, '3.0', sprintf(__('The <code>%1$s</code> plugin header is deprecated. Use <code>%2$s</code> instead.'), 'Site Wide Only: true', 'Network: true'));
            $plugin_data['Network'] = $plugin_data['_sitewide'];
        }
        $plugin_data['Network'] = ('true' == strtolower($plugin_data['Network']));
        unset($plugin_data['_sitewide']);

        if ($markup || $translate) {
            $plugin_data = $this->_get_plugin_data_markup_translate($plugin_file, $plugin_data, $markup, $translate);
        } else {
            $plugin_data['Title'] = $plugin_data['Name'];
            $plugin_data['AuthorName'] = $plugin_data['Author'];
        }

        return $plugin_data;
    }

    /**
     * Sanitizes plugin data, optionally adds markup, optionally translates.
     *
     * @since 2.7.0
     * @access private
     * @see get_plugin_data()
     */
    function _get_plugin_data_markup_translate($plugin_file, $plugin_data, $markup = true, $translate = true)
    {

        // Sanitize the plugin filename to a WP_PLUGIN_DIR relative path
        $plugin_file = plugin_basename($plugin_file);

        // Translate fields
        if ($translate) {
            if ($textdomain = $plugin_data['TextDomain']) {
                if ($plugin_data['DomainPath'])
                    load_plugin_textdomain($textdomain, false, dirname($plugin_file) . $plugin_data['DomainPath']);
                else
                    load_plugin_textdomain($textdomain, false, dirname($plugin_file));
            } elseif (in_array(basename($plugin_file), array('hello.php', 'akismet.php'))) {
                $textdomain = 'default';
            }
            if ($textdomain) {
                foreach (array('Name', 'PluginURI', 'Description', 'Author', 'AuthorURI', 'Version') as $field)
                    $plugin_data[$field] = translate($plugin_data[$field], $textdomain);
            }
        }

        // Sanitize fields
        $allowed_tags = $allowed_tags_in_links = array(
            'abbr' => array('title' => true),
            'acronym' => array('title' => true),
            'code' => true,
            'em' => true,
            'strong' => true,
        );
        $allowed_tags['a'] = array('href' => true, 'title' => true);

        // Name is marked up inside <a> tags. Don't allow these.
        // Author is too, but some plugins have used <a> here (omitting Author URI).
        $plugin_data['Name'] = wp_kses($plugin_data['Name'], $allowed_tags_in_links);
        $plugin_data['Author'] = wp_kses($plugin_data['Author'], $allowed_tags);

        $plugin_data['Description'] = wp_kses($plugin_data['Description'], $allowed_tags);
        $plugin_data['Version'] = wp_kses($plugin_data['Version'], $allowed_tags);

        $plugin_data['PluginURI'] = esc_url($plugin_data['PluginURI']);
        $plugin_data['AuthorURI'] = esc_url($plugin_data['AuthorURI']);

        $plugin_data['Title'] = $plugin_data['Name'];
        $plugin_data['AuthorName'] = $plugin_data['Author'];

        // Apply markup
        if ($markup) {
            if ($plugin_data['PluginURI'] && $plugin_data['Name'])
                $plugin_data['Title'] = '<a href="' . $plugin_data['PluginURI'] . '">' . $plugin_data['Name'] . '</a>';

            if ($plugin_data['AuthorURI'] && $plugin_data['Author'])
                $plugin_data['Author'] = '<a href="' . $plugin_data['AuthorURI'] . '">' . $plugin_data['Author'] . '</a>';

            $plugin_data['Description'] = wptexturize($plugin_data['Description']);

            if ($plugin_data['Author'])
                $plugin_data['Description'] .= ' <cite>' . sprintf(__('By %s.'), $plugin_data['Author']) . '</cite>';
        }

        return $plugin_data;
    }

    /**
     * Get a list of a plugin's files.
     *
     * @since 2.8.0
     *
     * @param string $plugin Plugin ID
     * @return array List of files relative to the plugin root.
     */
    function get_plugin_files($plugin)
    {
        $plugin_file = WP_PLUGIN_DIR . '/' . $plugin;
        $dir = dirname($plugin_file);
        $plugin_files = array($plugin);
        if (is_dir($dir) && $dir != WP_PLUGIN_DIR) {
            $plugins_dir = @ opendir($dir);
            if ($plugins_dir) {
                while (($file = readdir($plugins_dir)) !== false) {
                    if (substr($file, 0, 1) == '.')
                        continue;
                    if (is_dir($dir . '/' . $file)) {
                        $plugins_subdir = @ opendir($dir . '/' . $file);
                        if ($plugins_subdir) {
                            while (($subfile = readdir($plugins_subdir)) !== false) {
                                if (substr($subfile, 0, 1) == '.')
                                    continue;
                                $plugin_files[] = plugin_basename("$dir/$file/$subfile");
                            }
                            @closedir($plugins_subdir);
                        }
                    } else {
                        if (plugin_basename("$dir/$file") != $plugin)
                            $plugin_files[] = plugin_basename("$dir/$file");
                    }
                }
                @closedir($plugins_dir);
            }
        }

        return $plugin_files;
    }

    /**
     * Check the plugins directory and retrieve all plugin files with plugin data.
     *
     * WordPress only supports plugin files in the base plugins directory
     * (wp-content/plugins) and in one directory above the plugins directory
     * (wp-content/plugins/my-plugin). The file it looks for has the plugin data and
     * must be found in those two locations. It is recommended that do keep your
     * plugin files in directories.
     *
     * The file with the plugin data is the file that will be included and therefore
     * needs to have the main execution for the plugin. This does not mean
     * everything must be contained in the file and it is recommended that the file
     * be split for maintainability. Keep everything in one file for extreme
     * optimization purposes.
     *
     * @since 1.5.0
     *
     * @param string $plugin_folder Optional. Relative path to single plugin folder.
     * @return array Key is the plugin file path and the value is an array of the plugin data.
     */
    function get_plugins($plugin_folder = '')
    {

        if (!$cache_plugins = wp_cache_get('plugins', 'plugins'))
            $cache_plugins = array();

        if (isset($cache_plugins[$plugin_folder]))
            return $cache_plugins[$plugin_folder];

        $wp_plugins = array();
        $plugin_root = WP_PLUGIN_DIR;
        if (!empty($plugin_folder))
            $plugin_root .= $plugin_folder;

        // Files in wp-content/plugins directory
        $plugins_dir = @ opendir($plugin_root);
        $plugin_files = array();
        if ($plugins_dir) {
            while (($file = readdir($plugins_dir)) !== false) {
                if (substr($file, 0, 1) == '.')
                    continue;
                if (is_dir($plugin_root . '/' . $file)) {
                    $plugins_subdir = @ opendir($plugin_root . '/' . $file);
                    if ($plugins_subdir) {
                        while (($subfile = readdir($plugins_subdir)) !== false) {
                            if (substr($subfile, 0, 1) == '.')
                                continue;
                            if (substr($subfile, -4) == '.php')
                                $plugin_files[] = "$file/$subfile";
                        }
                        closedir($plugins_subdir);
                    }
                } else {
                    if (substr($file, -4) == '.php')
                        $plugin_files[] = $file;
                }
            }
            closedir($plugins_dir);
        }

        if (empty($plugin_files))
            return $wp_plugins;

        foreach ($plugin_files as $plugin_file) {
            if (!is_readable("$plugin_root/$plugin_file"))
                continue;

            $plugin_data = $this->get_plugin_data("$plugin_root/$plugin_file", false, false); //Do not apply markup/translate as it'll be cached.

            if (empty ($plugin_data['Name']))
                continue;

            $wp_plugins[plugin_basename($plugin_file)] = $plugin_data;
        }

//        uasort( $wp_plugins, '_sort_uname_callback' );

        $cache_plugins[$plugin_folder] = $wp_plugins;
        wp_cache_set('plugins', $cache_plugins, 'plugins');

        return $wp_plugins;
    }

    /**
     * Callback to sort array by a 'Name' key.
     *
     * @since 3.1.0
     * @access private
     */
    function _sort_uname_callback($a, $b)
    {
        return strnatcasecmp($a['Name'], $b['Name']);
    }

    function truncate($text, $chars = 50)
    {

        if ($chars != 0) {
            $text = $text . " ";
            $text = substr($text, 0, $chars);
            $text = substr($text, 0, strrpos($text, ' '));

            $text = $text . " ...";
        }

        return $text;
    }

    function getCurrencyInfo($currency_code)
    {

        $currencies = $this->currency_info();

        foreach ($currencies as $currencyCode => $val) :

            if ($currency_code == $currencyCode) {
                return $curr = array(
                    'symbol' => $val['symbol'],
                    'placement' => $val['symbol_placement']
                );
            }

        endforeach;

        return true;
    }


    function currency_info()
    {
        return array(
            'AED' => array(
                'code' => 'AED',
                'symbol' => 'د.إ',
                'name' => 'United Arab Emirates Dirham',
                'numeric_code' => '784',
                'code_placement' => 'before',
                'minor_unit' => 'Fils',
                'major_unit' => 'Dirham',
            ),
            'AFN' => array(
                'code' => 'AFN',
                'symbol' => 'Af',
                'name' => 'Afghan Afghani',
                'decimals' => 0,
                'numeric_code' => '971',
                'minor_unit' => 'Pul',
                'major_unit' => 'Afghani',
            ),
            'ANG' => array(
                'code' => 'ANG',
                'symbol' => 'NAf.',
                'name' => 'Netherlands Antillean Guilder',
                'numeric_code' => '532',
                'minor_unit' => 'Cent',
                'major_unit' => 'Guilder',
            ),
            'AOA' => array(
                'code' => 'AOA',
                'symbol' => 'Kz',
                'name' => 'Angolan Kwanza',
                'numeric_code' => '973',
                'minor_unit' => 'Cêntimo',
                'major_unit' => 'Kwanza',
            ),
            'ARM' => array(
                'code' => 'ARM',
                'symbol' => 'm$n',
                'name' => 'Argentine Peso Moneda Nacional',
                'minor_unit' => 'Centavos',
                'major_unit' => 'Peso',
            ),
            'ARS' => array(
                'code' => 'ARS',
                'symbol' => 'AR$',
                'name' => 'Argentine Peso',
                'numeric_code' => '032',
                'minor_unit' => 'Centavo',
                'major_unit' => 'Peso',
            ),
            'AUD' => array(
                'code' => 'AUD',
                'symbol' => 'AU$',
                'name' => 'Australian Dollar',
                'numeric_code' => '036',
                'minor_unit' => 'Cent',
                'major_unit' => 'Dollar',
            ),
            'AWG' => array(
                'code' => 'AWG',
                'symbol' => 'Afl.',
                'name' => 'Aruban Florin',
                'numeric_code' => '533',
                'minor_unit' => 'Cent',
                'major_unit' => 'Guilder',
            ),
            'AZN' => array(
                'code' => 'AZN',
                'symbol' => 'man.',
                'name' => 'Azerbaijanian Manat',
                'minor_unit' => 'Qəpik',
                'major_unit' => 'New Manat',
            ),
            'BAM' => array(
                'code' => 'BAM',
                'symbol' => 'KM',
                'name' => 'Bosnia-Herzegovina Convertible Mark',
                'numeric_code' => '977',
                'minor_unit' => 'Fening',
                'major_unit' => 'Convertible Marka',
            ),
            'BBD' => array(
                'code' => 'BBD',
                'symbol' => 'Bds$',
                'name' => 'Barbadian Dollar',
                'numeric_code' => '052',
                'minor_unit' => 'Cent',
                'major_unit' => 'Dollar',
            ),
            'BDT' => array(
                'code' => 'BDT',
                'symbol' => 'Tk',
                'name' => 'Bangladeshi Taka',
                'numeric_code' => '050',
                'minor_unit' => 'Paisa',
                'major_unit' => 'Taka',
            ),
            'BGN' => array(
                'code' => 'BGN',
                'symbol' => 'лв',
                'name' => 'Bulgarian lev',
                'thousands_separator' => ' ',
                'decimal_separator' => ',',
                'symbol_placement' => 'after',
                'code_placement' => '',
                'numeric_code' => '975',
                'minor_unit' => 'Stotinka',
                'major_unit' => 'Lev',
            ),
            'BHD' => array(
                'code' => 'BHD',
                'symbol' => 'BD',
                'name' => 'Bahraini Dinar',
                'decimals' => 3,
                'numeric_code' => '048',
                'minor_unit' => 'Fils',
                'major_unit' => 'Dinar',
            ),
            'BIF' => array(
                'code' => 'BIF',
                'symbol' => 'FBu',
                'name' => 'Burundian Franc',
                'decimals' => 0,
                'numeric_code' => '108',
                'minor_unit' => 'Centime',
                'major_unit' => 'Franc',
            ),
            'BMD' => array(
                'code' => 'BMD',
                'symbol' => 'BD$',
                'name' => 'Bermudan Dollar',
                'numeric_code' => '060',
                'minor_unit' => 'Cent',
                'major_unit' => 'Dollar',
            ),
            'BND' => array(
                'code' => 'BND',
                'symbol' => 'BN$',
                'name' => 'Brunei Dollar',
                'numeric_code' => '096',
                'minor_unit' => 'Sen',
                'major_unit' => 'Dollar',
            ),
            'BOB' => array(
                'code' => 'BOB',
                'symbol' => 'Bs',
                'name' => 'Bolivian Boliviano',
                'numeric_code' => '068',
                'minor_unit' => 'Centavo',
                'major_unit' => 'Bolivianos',
            ),
            'BRL' => array(
                'code' => 'BRL',
                'symbol' => 'R$',
                'name' => 'Brazilian Real',
                'numeric_code' => '986',
                'symbol_placement' => 'before',
                'code_placement' => '',
                'thousands_separator' => '.',
                'decimal_separator' => ',',
                'minor_unit' => 'Centavo',
                'major_unit' => 'Reais',
            ),
            'BSD' => array(
                'code' => 'BSD',
                'symbol' => 'BS$',
                'name' => 'Bahamian Dollar',
                'numeric_code' => '044',
                'minor_unit' => 'Cent',
                'major_unit' => 'Dollar',
            ),
            'BTN' => array(
                'code' => 'BTN',
                'symbol' => 'Nu.',
                'name' => 'Bhutanese Ngultrum',
                'numeric_code' => '064',
                'minor_unit' => 'Chetrum',
                'major_unit' => 'Ngultrum',
            ),
            'BWP' => array(
                'code' => 'BWP',
                'symbol' => 'BWP',
                'name' => 'Botswanan Pula',
                'numeric_code' => '072',
                'minor_unit' => 'Thebe',
                'major_unit' => 'Pulas',
            ),
            'BYR' => array(
                'code' => 'BYR',
                'symbol' => 'руб.',
                'name' => 'Belarusian ruble',
                'numeric_code' => '974',
                'symbol_placement' => 'after',
                'code_placement' => '',
                'decimals' => 0,
                'thousands_separator' => ' ',
                'major_unit' => 'Ruble',
            ),
            'BZD' => array(
                'code' => 'BZD',
                'symbol' => 'BZ$',
                'name' => 'Belize Dollar',
                'numeric_code' => '084',
                'minor_unit' => 'Cent',
                'major_unit' => 'Dollar',
            ),
            'CAD' => array(
                'code' => 'CAD',
                'symbol' => 'CA$',
                'name' => 'Canadian Dollar',
                'numeric_code' => '124',
                'minor_unit' => 'Cent',
                'major_unit' => 'Dollar',
            ),
            'CDF' => array(
                'code' => 'CDF',
                'symbol' => 'CDF',
                'name' => 'Congolese Franc',
                'numeric_code' => '976',
                'minor_unit' => 'Centime',
                'major_unit' => 'Franc',
            ),
            'CHF' => array(
                'code' => 'CHF',
                'symbol' => 'Fr.',
                'name' => 'Swiss Franc',
                'rounding_step' => '0.05',
                'numeric_code' => '756',
                'minor_unit' => 'Rappen',
                'major_unit' => 'Franc',
            ),
            'CLP' => array(
                'code' => 'CLP',
                'symbol' => 'CL$',
                'name' => 'Chilean Peso',
                'decimals' => 0,
                'numeric_code' => '152',
                'minor_unit' => 'Centavo',
                'major_unit' => 'Peso',
            ),
            'CNY' => array(
                'code' => 'CNY',
                'symbol' => 'CN¥',
                'name' => 'Chinese Yuan Renminbi',
                'numeric_code' => '156',
                'minor_unit' => 'Fe',
                'major_unit' => 'Yuan Renminbi',
            ),
            'COP' => array(
                'code' => 'COP',
                'symbol' => '$',
                'name' => 'Colombian Peso',
                'decimals' => 0,
                'numeric_code' => '170',
                'symbol_placement' => 'before',
                'code_placement' => 'hidden',
                'thousands_separator' => '.',
                'decimal_separator' => ',',
                'minor_unit' => 'Centavo',
                'major_unit' => 'Peso',
            ),
            'CRC' => array(
                'code' => 'CRC',
                'symbol' => '¢',
                'name' => 'Costa Rican Colón',
                'decimals' => 0,
                'numeric_code' => '188',
                'minor_unit' => 'Céntimo',
                'major_unit' => 'Colón',
            ),
            'CUC' => array(
                'code' => 'CUC',
                'symbol' => 'CUC$',
                'name' => 'Cuban Convertible Peso',
                'minor_unit' => 'Centavo',
                'major_unit' => 'Peso',
            ),
            'CUP' => array(
                'code' => 'CUP',
                'symbol' => 'CU$',
                'name' => 'Cuban Peso',
                'numeric_code' => '192',
                'minor_unit' => 'Centavo',
                'major_unit' => 'Peso',
            ),
            'CVE' => array(
                'code' => 'CVE',
                'symbol' => 'CV$',
                'name' => 'Cape Verdean Escudo',
                'numeric_code' => '132',
                'minor_unit' => 'Centavo',
                'major_unit' => 'Escudo',
            ),
            'CZK' => array(
                'code' => 'CZK',
                'symbol' => 'Kč',
                'name' => 'Czech Republic Koruna',
                'numeric_code' => '203',
                'thousands_separator' => ' ',
                'decimal_separator' => ',',
                'symbol_placement' => 'after',
                'code_placement' => '',
                'minor_unit' => 'Haléř',
                'major_unit' => 'Koruna',
            ),
            'DJF' => array(
                'code' => 'DJF',
                'symbol' => 'Fdj',
                'name' => 'Djiboutian Franc',
                'numeric_code' => '262',
                'decimals' => 0,
                'minor_unit' => 'Centime',
                'major_unit' => 'Franc',
            ),
            'DKK' => array(
                'code' => 'DKK',
                'symbol' => 'kr.',
                'name' => 'Danish Krone',
                'numeric_code' => '208',
                'thousands_separator' => ' ',
                'decimal_separator' => ',',
                'symbol_placement' => 'after',
                'code_placement' => '',
                'minor_unit' => 'Øre',
                'major_unit' => 'Kroner',
            ),
            'DOP' => array(
                'code' => 'DOP',
                'symbol' => 'RD$',
                'name' => 'Dominican Peso',
                'numeric_code' => '214',
                'minor_unit' => 'Centavo',
                'major_unit' => 'Peso',
            ),
            'DZD' => array(
                'code' => 'DZD',
                'symbol' => 'DA',
                'name' => 'Algerian Dinar',
                'numeric_code' => '012',
                'minor_unit' => 'Santeem',
                'major_unit' => 'Dinar',
            ),
            'EEK' => array(
                'code' => 'EEK',
                'symbol' => 'Ekr',
                'name' => 'Estonian Kroon',
                'thousands_separator' => ' ',
                'decimal_separator' => ',',
                'numeric_code' => '233',
                'minor_unit' => 'Sent',
                'major_unit' => 'Krooni',
            ),
            'EGP' => array(
                'code' => 'EGP',
                'symbol' => 'EG£',
                'name' => 'Egyptian Pound',
                'numeric_code' => '818',
                'minor_unit' => 'Piastr',
                'major_unit' => 'Pound',
            ),
            'ERN' => array(
                'code' => 'ERN',
                'symbol' => 'Nfk',
                'name' => 'Eritrean Nakfa',
                'numeric_code' => '232',
                'minor_unit' => 'Cent',
                'major_unit' => 'Nakfa',
            ),
            'ETB' => array(
                'code' => 'ETB',
                'symbol' => 'Br',
                'name' => 'Ethiopian Birr',
                'numeric_code' => '230',
                'minor_unit' => 'Santim',
                'major_unit' => 'Birr',
            ),
            'EUR' => array(
                'code' => 'EUR',
                'symbol' => '€',
                'name' => 'Euro',
                'thousands_separator' => ' ',
                'decimal_separator' => ',',
                'symbol_placement' => 'after',
                'code_placement' => '',
                'numeric_code' => '978',
                'minor_unit' => 'Cent',
                'major_unit' => 'Euro',
            ),
            'FJD' => array(
                'code' => 'FJD',
                'symbol' => 'FJ$',
                'name' => 'Fijian Dollar',
                'numeric_code' => '242',
                'minor_unit' => 'Cent',
                'major_unit' => 'Dollar',
            ),
            'FKP' => array(
                'code' => 'FKP',
                'symbol' => 'FK£',
                'name' => 'Falkland Islands Pound',
                'numeric_code' => '238',
                'minor_unit' => 'Penny',
                'major_unit' => 'Pound',
            ),
            'GBP' => array(
                'code' => 'GBP',
                'symbol' => '£',
                'name' => 'British Pound Sterling',
                'numeric_code' => '826',
                'symbol_placement' => 'before',
                'code_placement' => '',
                'minor_unit' => 'Penny',
                'major_unit' => 'Pound',
            ),
            'GHS' => array(
                'code' => 'GHS',
                'symbol' => 'GH₵',
                'name' => 'Ghanaian Cedi',
                'minor_unit' => 'Pesewa',
                'major_unit' => 'Cedi',
            ),
            'GIP' => array(
                'code' => 'GIP',
                'symbol' => 'GI£',
                'name' => 'Gibraltar Pound',
                'numeric_code' => '292',
                'minor_unit' => 'Penny',
                'major_unit' => 'Pound',
            ),
            'GMD' => array(
                'code' => 'GMD',
                'symbol' => 'GMD',
                'name' => 'Gambian Dalasi',
                'numeric_code' => '270',
                'minor_unit' => 'Butut',
                'major_unit' => 'Dalasis',
            ),
            'GNF' => array(
                'code' => 'GNF',
                'symbol' => 'FG',
                'name' => 'Guinean Franc',
                'decimals' => 0,
                'numeric_code' => '324',
                'minor_unit' => 'Centime',
                'major_unit' => 'Franc',
            ),
            'GTQ' => array(
                'code' => 'GTQ',
                'symbol' => 'GTQ',
                'name' => 'Guatemalan Quetzal',
                'numeric_code' => '320',
                'minor_unit' => 'Centavo',
                'major_unit' => 'Quetzales',
            ),
            'GYD' => array(
                'code' => 'GYD',
                'symbol' => 'GY$',
                'name' => 'Guyanaese Dollar',
                'decimals' => 0,
                'numeric_code' => '328',
                'minor_unit' => 'Cent',
                'major_unit' => 'Dollar',
            ),
            'HKD' => array(
                'code' => 'HKD',
                'symbol' => 'HK$',
                'name' => 'Hong Kong Dollar',
                'numeric_code' => '344',
                'symbol_placement' => 'before',
                'code_placement' => '',
                'minor_unit' => 'Cent',
                'major_unit' => 'Dollar',
            ),
            'HNL' => array(
                'code' => 'HNL',
                'symbol' => 'HNL',
                'name' => 'Honduran Lempira',
                'numeric_code' => '340',
                'minor_unit' => 'Centavo',
                'major_unit' => 'Lempiras',
            ),
            'HRK' => array(
                'code' => 'HRK',
                'symbol' => 'kn',
                'name' => 'Croatian Kuna',
                'numeric_code' => '191',
                'minor_unit' => 'Lipa',
                'major_unit' => 'Kuna',
            ),
            'HTG' => array(
                'code' => 'HTG',
                'symbol' => 'HTG',
                'name' => 'Haitian Gourde',
                'numeric_code' => '332',
                'minor_unit' => 'Centime',
                'major_unit' => 'Gourde',
            ),
            'HUF' => array(
                'code' => 'HUF',
                'symbol' => 'Ft',
                'name' => 'Hungarian Forint',
                'numeric_code' => '348',
                'decimal_separator' => ',',
                'thousands_separator' => ' ',
                'decimals' => 0,
                'symbol_placement' => 'after',
                'code_placement' => '',
                'major_unit' => 'Forint',
            ),
            'IDR' => array(
                'code' => 'IDR',
                'symbol' => 'Rp',
                'name' => 'Indonesian Rupiah',
                'decimals' => 0,
                'numeric_code' => '360',
                'minor_unit' => 'Sen',
                'major_unit' => 'Rupiahs',
            ),
            'ILS' => array(
                'code' => 'ILS',
                'symbol' => '₪',
                'name' => 'Israeli New Shekel',
                'numeric_code' => '376',
                'symbol_placement' => 'before',
                'code_placement' => '',
                'minor_unit' => 'Agora',
                'major_unit' => 'New Shekels',
            ),
            'INR' => array(
                'code' => 'INR',
                'symbol' => 'Rs',
                'name' => 'Indian Rupee',
                'numeric_code' => '356',
                'minor_unit' => 'Paisa',
                'major_unit' => 'Rupee',
            ),
            'IRR' => array(
                'code' => 'IRR',
                'symbol' => '﷼',
                'name' => 'Iranian Rial',
                'numeric_code' => '364',
                'symbol_placement' => 'after',
                'code_placement' => '',
                'minor_unit' => 'Rial',
                'major_unit' => 'Toman',
            ),
            'ISK' => array(
                'code' => 'ISK',
                'symbol' => 'Ikr',
                'name' => 'Icelandic Króna',
                'decimals' => 0,
                'thousands_separator' => ' ',
                'numeric_code' => '352',
                'minor_unit' => 'Eyrir',
                'major_unit' => 'Kronur',
            ),
            'JMD' => array(
                'code' => 'JMD',
                'symbol' => 'J$',
                'name' => 'Jamaican Dollar',
                'numeric_code' => '388',
                'symbol_placement' => 'before',
                'code_placement' => '',
                'minor_unit' => 'Cent',
                'major_unit' => 'Dollar',
            ),
            'JOD' => array(
                'code' => 'JOD',
                'symbol' => 'JD',
                'name' => 'Jordanian Dinar',
                'decimals' => 3,
                'numeric_code' => '400',
                'minor_unit' => 'Piastr',
                'major_unit' => 'Dinar',
            ),
            'JPY' => array(
                'code' => 'JPY',
                'symbol' => '¥',
                'name' => 'Japanese Yen',
                'decimals' => 0,
                'numeric_code' => '392',
                'symbol_placement' => 'before',
                'code_placement' => '',
                'minor_unit' => 'Sen',
                'major_unit' => 'Yen',
            ),
            'KES' => array(
                'code' => 'KES',
                'symbol' => 'Ksh',
                'name' => 'Kenyan Shilling',
                'numeric_code' => '404',
                'minor_unit' => 'Cent',
                'major_unit' => 'Shilling',
            ),
            'KGS' => array(
                'code' => 'KGS',
                'code_placement' => '',
                'symbol' => 'сом',
                'symbol_placement' => 'after',
                'name' => 'Kyrgyzstani Som',
                'numeric_code' => '417',
                'thousands_separator' => '',
                'major_unit' => 'Som',
                'minor_unit' => 'Tyiyn',
            ),
            'KMF' => array(
                'code' => 'KMF',
                'symbol' => 'CF',
                'name' => 'Comorian Franc',
                'decimals' => 0,
                'numeric_code' => '174',
                'minor_unit' => 'Centime',
                'major_unit' => 'Franc',
            ),
            'KRW' => array(
                'code' => 'KRW',
                'symbol' => '₩',
                'name' => 'South Korean Won',
                'decimals' => 0,
                'numeric_code' => '410',
                'minor_unit' => 'Jeon',
                'major_unit' => 'Won',
            ),
            'KWD' => array(
                'code' => 'KWD',
                'symbol' => 'KD',
                'name' => 'Kuwaiti Dinar',
                'decimals' => 3,
                'numeric_code' => '414',
                'minor_unit' => 'Fils',
                'major_unit' => 'Dinar',
            ),
            'KYD' => array(
                'code' => 'KYD',
                'symbol' => 'KY$',
                'name' => 'Cayman Islands Dollar',
                'numeric_code' => '136',
                'minor_unit' => 'Cent',
                'major_unit' => 'Dollar',
            ),
            'KZT' => array(
                'code' => 'KZT',
                'symbol' => 'тг.',
                'name' => 'Kazakhstani tenge',
                'numeric_code' => '398',
                'thousands_separator' => ' ',
                'decimal_separator' => ',',
                'symbol_placement' => 'after',
                'code_placement' => '',
                'minor_unit' => 'Tiyn',
                'major_unit' => 'Tenge',
            ),
            'LAK' => array(
                'code' => 'LAK',
                'symbol' => '₭N',
                'name' => 'Laotian Kip',
                'decimals' => 0,
                'numeric_code' => '418',
                'minor_unit' => 'Att',
                'major_unit' => 'Kips',
            ),
            'LBP' => array(
                'code' => 'LBP',
                'symbol' => 'LB£',
                'name' => 'Lebanese Pound',
                'decimals' => 0,
                'numeric_code' => '422',
                'minor_unit' => 'Piastre',
                'major_unit' => 'Pound',
            ),
            'LKR' => array(
                'code' => 'LKR',
                'symbol' => 'SLRs',
                'name' => 'Sri Lanka Rupee',
                'numeric_code' => '144',
                'minor_unit' => 'Cent',
                'major_unit' => 'Rupee',
            ),
            'LRD' => array(
                'code' => 'LRD',
                'symbol' => 'L$',
                'name' => 'Liberian Dollar',
                'numeric_code' => '430',
                'minor_unit' => 'Cent',
                'major_unit' => 'Dollar',
            ),
            'LSL' => array(
                'code' => 'LSL',
                'symbol' => 'LSL',
                'name' => 'Lesotho Loti',
                'numeric_code' => '426',
                'minor_unit' => 'Sente',
                'major_unit' => 'Loti',
            ),
            'LTL' => array(
                'code' => 'LTL',
                'symbol' => 'Lt',
                'name' => 'Lithuanian Litas',
                'numeric_code' => '440',
                'minor_unit' => 'Centas',
                'major_unit' => 'Litai',
            ),
            'LVL' => array(
                'code' => 'LVL',
                'symbol' => 'Ls',
                'name' => 'Latvian Lats',
                'numeric_code' => '428',
                'minor_unit' => 'Santims',
                'major_unit' => 'Lati',
            ),
            'LYD' => array(
                'code' => 'LYD',
                'symbol' => 'LD',
                'name' => 'Libyan Dinar',
                'decimals' => 3,
                'numeric_code' => '434',
                'minor_unit' => 'Dirham',
                'major_unit' => 'Dinar',
            ),
            'MAD' => array(
                'code' => 'MAD',
                'symbol' => ' Dhs',
                'name' => 'Moroccan Dirham',
                'numeric_code' => '504',
                'symbol_placement' => 'after',
                'code_placement' => '',
                'minor_unit' => 'Santimat',
                'major_unit' => 'Dirhams',
            ),
            'MDL' => array(
                'code' => 'MDL',
                'symbol' => 'MDL',
                'name' => 'Moldovan leu',
                'symbol_placement' => 'after',
                'numeric_code' => '498',
                'code_placement' => '',
                'minor_unit' => 'bani',
                'major_unit' => 'Lei',
            ),
            'MMK' => array(
                'code' => 'MMK',
                'symbol' => 'MMK',
                'name' => 'Myanma Kyat',
                'decimals' => 0,
                'numeric_code' => '104',
                'minor_unit' => 'Pya',
                'major_unit' => 'Kyat',
            ),
            'MNT' => array(
                'code' => 'MNT',
                'symbol' => '₮',
                'name' => 'Mongolian Tugrik',
                'decimals' => 0,
                'numeric_code' => '496',
                'minor_unit' => 'Möngö',
                'major_unit' => 'Tugriks',
            ),
            'MOP' => array(
                'code' => 'MOP',
                'symbol' => 'MOP$',
                'name' => 'Macanese Pataca',
                'numeric_code' => '446',
                'minor_unit' => 'Avo',
                'major_unit' => 'Pataca',
            ),
            'MRO' => array(
                'code' => 'MRO',
                'symbol' => 'UM',
                'name' => 'Mauritanian Ouguiya',
                'decimals' => 0,
                'numeric_code' => '478',
                'minor_unit' => 'Khoums',
                'major_unit' => 'Ouguiya',
            ),
            'MTP' => array(
                'code' => 'MTP',
                'symbol' => 'MT£',
                'name' => 'Maltese Pound',
                'minor_unit' => 'Shilling',
                'major_unit' => 'Pound',
            ),
            'MUR' => array(
                'code' => 'MUR',
                'symbol' => 'MURs',
                'name' => 'Mauritian Rupee',
                'decimals' => 0,
                'numeric_code' => '480',
                'minor_unit' => 'Cent',
                'major_unit' => 'Rupee',
            ),
            'MXN' => array(
                'code' => 'MXN',
                'symbol' => '$',
                'name' => 'Mexican Peso',
                'numeric_code' => '484',
                'symbol_placement' => 'before',
                'code_placement' => '',
                'minor_unit' => 'Centavo',
                'major_unit' => 'Peso',
            ),
            'MYR' => array(
                'code' => 'MYR',
                'symbol' => 'RM',
                'name' => 'Malaysian Ringgit',
                'numeric_code' => '458',
                'symbol_placement' => 'before',
                'code_placement' => '',
                'minor_unit' => 'Sen',
                'major_unit' => 'Ringgits',
            ),
            'MZN' => array(
                'code' => 'MZN',
                'symbol' => 'MTn',
                'name' => 'Mozambican Metical',
                'minor_unit' => 'Centavo',
                'major_unit' => 'Metical',
            ),
            'NAD' => array(
                'code' => 'NAD',
                'symbol' => 'N$',
                'name' => 'Namibian Dollar',
                'numeric_code' => '516',
                'minor_unit' => 'Cent',
                'major_unit' => 'Dollar',
            ),
            'NGN' => array(
                'code' => 'NGN',
                'symbol' => '₦',
                'name' => 'Nigerian Naira',
                'numeric_code' => '566',
                'minor_unit' => 'Kobo',
                'major_unit' => 'Naira',
            ),
            'NIO' => array(
                'code' => 'NIO',
                'symbol' => 'C$',
                'name' => 'Nicaraguan Cordoba Oro',
                'numeric_code' => '558',
                'minor_unit' => 'Centavo',
                'major_unit' => 'Cordoba',
            ),
            'NOK' => array(
                'code' => 'NOK',
                'symbol' => 'Nkr',
                'name' => 'Norwegian Krone',
                'thousands_separator' => ' ',
                'decimal_separator' => ',',
                'numeric_code' => '578',
                'minor_unit' => 'Øre',
                'major_unit' => 'Krone',
            ),
            'NPR' => array(
                'code' => 'NPR',
                'symbol' => 'NPRs',
                'name' => 'Nepalese Rupee',
                'numeric_code' => '524',
                'minor_unit' => 'Paisa',
                'major_unit' => 'Rupee',
            ),
            'NZD' => array(
                'code' => 'NZD',
                'symbol' => 'NZ$',
                'name' => 'New Zealand Dollar',
                'numeric_code' => '554',
                'minor_unit' => 'Cent',
                'major_unit' => 'Dollar',
            ),
            'PAB' => array(
                'code' => 'PAB',
                'symbol' => 'B/.',
                'name' => 'Panamanian Balboa',
                'numeric_code' => '590',
                'minor_unit' => 'Centésimo',
                'major_unit' => 'Balboa',
            ),
            'PEN' => array(
                'code' => 'PEN',
                'symbol' => 'S/.',
                'name' => 'Peruvian Nuevo Sol',
                'numeric_code' => '604',
                'symbol_placement' => 'before',
                'code_placement' => '',
                'minor_unit' => 'Céntimo',
                'major_unit' => 'Nuevos Sole',
            ),
            'PGK' => array(
                'code' => 'PGK',
                'symbol' => 'PGK',
                'name' => 'Papua New Guinean Kina',
                'numeric_code' => '598',
                'minor_unit' => 'Toea',
                'major_unit' => 'Kina ',
            ),
            'PHP' => array(
                'code' => 'PHP',
                'symbol' => '₱',
                'name' => 'Philippine Peso',
                'numeric_code' => '608',
                'minor_unit' => 'Centavo',
                'major_unit' => 'Peso',
            ),
            'PKR' => array(
                'code' => 'PKR',
                'symbol' => 'PKRs',
                'name' => 'Pakistani Rupee',
                'decimals' => 0,
                'numeric_code' => '586',
                'minor_unit' => 'Paisa',
                'major_unit' => 'Rupee',
            ),
            'PLN' => array(
                'code' => 'PLN',
                'symbol' => 'zł',
                'name' => 'Polish Złoty',
                'decimal_separator' => ',',
                'thousands_separator' => ' ',
                'numeric_code' => '985',
                'symbol_placement' => 'after',
                'code_placement' => '',
                'minor_unit' => 'Grosz',
                'major_unit' => 'Złotych',
            ),
            'PYG' => array(
                'code' => 'PYG',
                'symbol' => '₲',
                'name' => 'Paraguayan Guarani',
                'decimals' => 0,
                'numeric_code' => '600',
                'minor_unit' => 'Céntimo',
                'major_unit' => 'Guarani',
            ),
            'QAR' => array(
                'code' => 'QAR',
                'symbol' => 'QR',
                'name' => 'Qatari Rial',
                'numeric_code' => '634',
                'minor_unit' => 'Dirham',
                'major_unit' => 'Rial',
            ),
            'RHD' => array(
                'code' => 'RHD',
                'symbol' => 'RH$',
                'name' => 'Rhodesian Dollar',
                'minor_unit' => 'Cent',
                'major_unit' => 'Dollar',
            ),
            'RON' => array(
                'code' => 'RON',
                'symbol' => 'RON',
                'name' => 'Romanian Leu',
                'minor_unit' => 'Ban',
                'major_unit' => 'Leu',
            ),
            'RSD' => array(
                'code' => 'RSD',
                'symbol' => 'din.',
                'name' => 'Serbian Dinar',
                'decimals' => 0,
                'minor_unit' => 'Para',
                'major_unit' => 'Dinars',
            ),
            'RUB' => array(
                'code' => 'RUB',
                'symbol' => 'руб.',
                'name' => 'Russian Ruble',
                'thousands_separator' => ' ',
                'decimal_separator' => ',',
                'numeric_code' => '643',
                'symbol_placement' => 'after',
                'code_placement' => '',
                'minor_unit' => 'Kopek',
                'major_unit' => 'Ruble',
            ),
            'SAR' => array(
                'code' => 'SAR',
                'symbol' => 'SR',
                'name' => 'Saudi Riyal',
                'numeric_code' => '682',
                'minor_unit' => 'Hallallah',
                'major_unit' => 'Riyals',
            ),
            'SBD' => array(
                'code' => 'SBD',
                'symbol' => 'SI$',
                'name' => 'Solomon Islands Dollar',
                'numeric_code' => '090',
                'minor_unit' => 'Cent',
                'major_unit' => 'Dollar',
            ),
            'SCR' => array(
                'code' => 'SCR',
                'symbol' => 'SRe',
                'name' => 'Seychellois Rupee',
                'numeric_code' => '690',
                'minor_unit' => 'Cent',
                'major_unit' => 'Rupee',
            ),
            'SDD' => array(
                'code' => 'SDD',
                'symbol' => 'LSd',
                'name' => 'Old Sudanese Dinar',
                'numeric_code' => '736',
                'minor_unit' => 'None',
                'major_unit' => 'Dinar',
            ),
            'SEK' => array(
                'code' => 'SEK',
                'symbol' => 'kr',
                'name' => 'Swedish Krona',
                'numeric_code' => '752',
                'thousands_separator' => ' ',
                'decimal_separator' => ',',
                'symbol_placement' => 'after',
                'code_placement' => '',
                'minor_unit' => 'Öre',
                'major_unit' => 'Kronor',
            ),
            'SGD' => array(
                'code' => 'SGD',
                'symbol' => 'S$',
                'name' => 'Singapore Dollar',
                'numeric_code' => '702',
                'minor_unit' => 'Cent',
                'major_unit' => 'Dollar',
            ),
            'SHP' => array(
                'code' => 'SHP',
                'symbol' => 'SH£',
                'name' => 'Saint Helena Pound',
                'numeric_code' => '654',
                'minor_unit' => 'Penny',
                'major_unit' => 'Pound',
            ),
            'SLL' => array(
                'code' => 'SLL',
                'symbol' => 'Le',
                'name' => 'Sierra Leonean Leone',
                'decimals' => 0,
                'numeric_code' => '694',
                'minor_unit' => 'Cent',
                'major_unit' => 'Leone',
            ),
            'SOS' => array(
                'code' => 'SOS',
                'symbol' => 'Ssh',
                'name' => 'Somali Shilling',
                'decimals' => 0,
                'numeric_code' => '706',
                'minor_unit' => 'Cent',
                'major_unit' => 'Shilling',
            ),
            'SRD' => array(
                'code' => 'SRD',
                'symbol' => 'SR$',
                'name' => 'Surinamese Dollar',
                'minor_unit' => 'Cent',
                'major_unit' => 'Dollar',
            ),
            'SRG' => array(
                'code' => 'SRG',
                'symbol' => 'Sf',
                'name' => 'Suriname Guilder',
                'numeric_code' => '740',
                'minor_unit' => 'Cent',
                'major_unit' => 'Guilder',
            ),
            'STD' => array(
                'code' => 'STD',
                'symbol' => 'Db',
                'name' => 'São Tomé and Príncipe Dobra',
                'decimals' => 0,
                'numeric_code' => '678',
                'minor_unit' => 'Cêntimo',
                'major_unit' => 'Dobra',
            ),
            'SYP' => array(
                'code' => 'SYP',
                'symbol' => 'SY£',
                'name' => 'Syrian Pound',
                'decimals' => 0,
                'numeric_code' => '760',
                'minor_unit' => 'Piastre',
                'major_unit' => 'Pound',
            ),
            'SZL' => array(
                'code' => 'SZL',
                'symbol' => 'SZL',
                'name' => 'Swazi Lilangeni',
                'numeric_code' => '748',
                'minor_unit' => 'Cent',
                'major_unit' => 'Lilangeni',
            ),
            'THB' => array(
                'code' => 'THB',
                'symbol' => '฿',
                'name' => 'Thai Baht',
                'numeric_code' => '764',
                'minor_unit' => 'Satang',
                'major_unit' => 'Baht',
            ),
            'TND' => array(
                'code' => 'TND',
                'symbol' => 'DT',
                'name' => 'Tunisian Dinar',
                'decimals' => 3,
                'numeric_code' => '788',
                'minor_unit' => 'Millime',
                'major_unit' => 'Dinar',
            ),
            'TOP' => array(
                'code' => 'TOP',
                'symbol' => 'T$',
                'name' => 'Tongan Paʻanga',
                'numeric_code' => '776',
                'minor_unit' => 'Senit',
                'major_unit' => 'Paʻanga',
            ),
            'TRY' => array(
                'code' => 'TRY',
                'symbol' => 'TL',
                'name' => 'Turkish Lira',
                'minor_unit' => 'Kurus',
                'major_unit' => 'Lira',
            ),
            'TTD' => array(
                'code' => 'TTD',
                'symbol' => 'TT$',
                'name' => 'Trinidad and Tobago Dollar',
                'numeric_code' => '780',
                'minor_unit' => 'Cent',
                'major_unit' => 'Dollar',
            ),
            'TWD' => array(
                'code' => 'TWD',
                'symbol' => 'NT$',
                'name' => 'New Taiwan Dollar',
                'numeric_code' => '901',
                'minor_unit' => 'Cent',
                'major_unit' => 'New Dollar',
            ),
            'TZS' => array(
                'code' => 'TZS',
                'symbol' => 'TSh',
                'name' => 'Tanzanian Shilling',
                'decimals' => 0,
                'numeric_code' => '834',
                'minor_unit' => 'Senti',
                'major_unit' => 'Shilling',
            ),
            'UAH' => array(
                'code' => 'UAH',
                'symbol' => 'грн.',
                'name' => 'Ukrainian Hryvnia',
                'numeric_code' => '980',
                'thousands_separator' => '',
                'decimal_separator' => '.',
                'symbol_placement' => 'after',
                'code_placement' => '',
                'minor_unit' => 'Kopiyka',
                'major_unit' => 'Hryvnia',
            ),
            'UGX' => array(
                'code' => 'UGX',
                'symbol' => 'USh',
                'name' => 'Ugandan Shilling',
                'decimals' => 0,
                'numeric_code' => '800',
                'minor_unit' => 'Cent',
                'major_unit' => 'Shilling',
            ),
            'USD' => array(
                'code' => 'USD',
                'symbol' => '$',
                'name' => 'United States Dollar',
                'numeric_code' => '840',
                'symbol_placement' => 'before',
                'code_placement' => '',
                'minor_unit' => 'Cent',
                'major_unit' => 'Dollar',
            ),
            'UYU' => array(
                'code' => 'UYU',
                'symbol' => '$U',
                'name' => 'Uruguayan Peso',
                'numeric_code' => '858',
                'minor_unit' => 'Centésimo',
                'major_unit' => 'Peso',
            ),
            'VEF' => array(
                'code' => 'VEF',
                'symbol' => 'Bs.F.',
                'name' => 'Venezuelan Bolívar Fuerte',
                'minor_unit' => 'Céntimo',
                'major_unit' => 'Bolivares Fuerte',
            ),
            'VND' => array(
                'code' => 'VND',
                'symbol' => 'đ',
                'name' => 'Vietnamese Dong',
                'decimals' => 0,
                'thousands_separator' => '.',
                'symbol_placement' => 'after',
                'symbol_spacer' => '',
                'code_placement' => '',
                'numeric_code' => '704',
                'minor_unit' => 'Hà',
                'major_unit' => 'Dong',
            ),
            'VUV' => array(
                'code' => 'VUV',
                'symbol' => 'VT',
                'name' => 'Vanuatu Vatu',
                'decimals' => 0,
                'numeric_code' => '548',
                'major_unit' => 'Vatu',
            ),
            'WST' => array(
                'code' => 'WST',
                'symbol' => 'WS$',
                'name' => 'Samoan Tala',
                'numeric_code' => '882',
                'minor_unit' => 'Sene',
                'major_unit' => 'Tala',
            ),
            'XAF' => array(
                'code' => 'XAF',
                'symbol' => 'FCFA',
                'name' => 'CFA Franc BEAC',
                'decimals' => 0,
                'numeric_code' => '950',
                'minor_unit' => 'Centime',
                'major_unit' => 'Franc',
            ),
            'XCD' => array(
                'code' => 'XCD',
                'symbol' => 'EC$',
                'name' => 'East Caribbean Dollar',
                'numeric_code' => '951',
                'minor_unit' => 'Cent',
                'major_unit' => 'Dollar',
            ),
            'XOF' => array(
                'code' => 'XOF',
                'symbol' => 'CFA',
                'name' => 'CFA Franc BCEAO',
                'decimals' => 0,
                'numeric_code' => '952',
                'minor_unit' => 'Centime',
                'major_unit' => 'Franc',
            ),
            'XPF' => array(
                'code' => 'XPF',
                'symbol' => 'CFPF',
                'name' => 'CFP Franc',
                'decimals' => 0,
                'numeric_code' => '953',
                'minor_unit' => 'Centime',
                'major_unit' => 'Franc',
            ),
            'YER' => array(
                'code' => 'YER',
                'symbol' => 'YR',
                'name' => 'Yemeni Rial',
                'decimals' => 0,
                'numeric_code' => '886',
                'minor_unit' => 'Fils',
                'major_unit' => 'Rial',
            ),
            'ZAR' => array(
                'code' => 'ZAR',
                'symbol' => 'R',
                'name' => 'South African Rand',
                'numeric_code' => '710',
                'symbol_placement' => 'before',
                'code_placement' => '',
                'minor_unit' => 'Cent',
                'major_unit' => 'Rand',
            ),
            'ZMK' => array(
                'code' => 'ZMK',
                'symbol' => 'ZK',
                'name' => 'Zambian Kwacha',
                'decimals' => 0,
                'numeric_code' => '894',
                'minor_unit' => 'Ngwee',
                'major_unit' => 'Kwacha',
            ),
        );
    }

}

function Etsy360_Helper_Class()
{
    return Etsy360_Helpers::instance();
}

// Get Plugin Running
Etsy360_Helper_Class();