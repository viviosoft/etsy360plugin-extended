<?php

// Exit if accessed directly
if (!defined('ABSPATH')) exit;

class Etsy_API extends Etsy360_Helpers
{

    function __construct()
    {

    }

    public function getShopListings()
    {

        $page = ($_GET['current_page']) ? $_GET['current_page'] : 1;
//        $items_per_page = ($this->get_wp_option('items_per_page') == "" || $this->get_wp_option('items_per_page') == null || $this->get_wp_option('items_per_page') == 0) ? 20 : $this->get_wp_option('items_per_page');

        $activeListings = $this->_getRequest("https://openapi.etsy.com/v2/shops/__SELF__/listings/active?includes=Images&page=$page");

        return $activeListings;
    }

    public function getShopFeatured()
    {

        $shopSections = $this->_getRequest("https://openapi.etsy.com/v2/shops/__SELF__/listings/featured?includes=Shop,Images");
        return $shopSections;
    }


    public function getShopSectionListings($sectionId)
    {

        $shopSections = $this->_getRequest("https://openapi.etsy.com/v2/shops/__SELF__/sections/$sectionId/listings/active?includes=Listings,Images&limit=100");
        return $shopSections;

    }


    public function getListingDetails($listingId = null)
    {

        $itemDetails = $this->_getRequest("https://openapi.etsy.com/v2/listings/$listingId?includes=Images,ShippingInfo,PaymentInfo,Variations");
        return $itemDetails->results;

    }


    /*
        Used for caching
    */

    public function getShopInformation()
    {

        $shopInfo = $this->_getRequest('https://openapi.etsy.com/v2/shops/__SELF__/');
        return $shopInfo->results;

    }

    public function getSalesTransactions()
    {

        $sales = $this->_getRequest('https://openapi.etsy.com/v2/shops/__SELF__/receipts/?includes=Transactions');

        echo '<pre>';
        echo print_r($sales);
        echo '</pre>';

        exit;

        return $sales->results;

    }

    public function getShopSections()
    {

        $shopSections = $this->_getRequest('https://openapi.etsy.com/v2/shops/__SELF__/sections/');
        return $shopSections->results;

    }

    public function getShopItems($offset, $user)
    {

        $activeListings = $this->_getRequest("https://openapi.etsy.com/v2/shops/__SELF__/listings/active?includes=Images&page=$offset");
        return $activeListings;

    }

    private function _getRequest($uri)
    {

        if (class_exists('Etsy360OAuth\Etsy360_oAuth')) {

            if ($success = oauth_etsy_class()->Initialize()) {
                oauth_etsy_class()->CallAPI(
                    $uri,
                    'GET', array(), array('FailOnAccessError' => true), $returnRequest);
                $success = oauth_etsy_class()->Finalize($success);
            }

            if ($success) {

                if ($this->get_wp_option('debugging') == 1) {
                    $debug = new PHPDebugger();
                    $debug->var2console($returnRequest, 'API SUCCESS ', true);
                }

                return $returnRequest;
            } else {

                if ($this->get_wp_option('debugging') == 1) {
                    $debug = new PHPDebugger();
                    $debug->var2console($returnRequest, 'API ERROR : ', true);
                }

                return $returnRequest;
            }

        } else {

            $debug = new PHPDebugger();
            $debug->var2console('OAuth Plugin not enabled', ' ', true);
            return true;
        }

    }

}

//$etsyAPI = new Etsy_API();
