<?php

//echo print_r($itemListings->results);

if (isset($itemListings->results) && !empty($itemListings->results)) {

    $content .= '<div class="section group">';
    $num_rows = Etsy360_Helper_Class()->get_wp_option('num_rows');

    switch ($num_rows) {
        case 'e360_span_3' :
            $row_count = 3;
            break;
        case 'e360_span_4' :
            $row_count = 4;
            break;
        case 'e360_span_5' :
            $row_count = 5;
            break;
        default :
            $row_count = 3;
            break;
    }

    $truncate_title = (Etsy360_Helper_Class()->get_wp_option('num_char') != '') ? Etsy360_Helper_Class()->get_wp_option('num_char') : 20;

    $image_height = (Etsy360_Helper_Class()->get_wp_option('image_height') != '') ? Etsy360_Helper_Class()->get_wp_option('image_height') : '';

    $content .= '<div class="e360-grid e360-grid-pad">';


    foreach ($itemListings->results as $d) {

        $shopCurrency = $this->getCurrencyInfo($d->currency_code);

        $content .= '<div class="e360Col-1-' . $row_count . ' mobile-e360Col-1-1">';

        $content .= '<div class="image-wrapper" >
                    <a class="item-image-anchor" href="' . $selectedPage . '?item-details=' . $d->listing_id . '&item-name=' . Etsy360_Helper_Class()->clean_item_url_string($d->title) . '" >
                    <img title="' . $d->title . '" alt="' . $d->title . '" class="item-image crop" data-crop-image-ratio=".9" src="' . $d->Images[0]->url_570xN . '" /></a>';

        if (Etsy360_Helper_Class()->get_wp_option('e_theme') == 'modern' || Etsy360_Helper_Class()->get_wp_option('e_theme') == '') {
            $content .= '<div class="item-price price-margin-3" style="background-color:' . Etsy360_Helper_Class()->get_wp_option('accent_color') . '">';
            $content .= ($shopCurrency['placement'] == 'before') ? $shopCurrency['symbol'] . $d->price : $d->price . " " . $shopCurrency['symbol'];
            $content .= '</div>';
        }

        $content .= '</div>';

        $content .= '<div class="item-details">';

        $content .= '<div class="item-title">';
        $content .= '<a alt="' . $d->title . '" title="' . $d->title . '" href="' . $selectedPage . '?item-details=' . $d->listing_id . '&item-name=' . Etsy360_Helper_Class()->clean_item_url_string($d->title) . '" style="color: ' . Etsy360_Helper_Class()->get_wp_option('accent_color') . '">' . Etsy360_Helper_Class()->truncate($d->title, $truncate_title) . '</a>';
        $content .= '</div>';

        if (Etsy360_Helper_Class()->get_wp_option('e_theme') == 'classic') {
            $content .= '<div class="item-price price-margin-3" style="background-color:' . Etsy360_Helper_Class()->get_wp_option('accent_color') . '">';
            $content .= ($shopCurrency['placement'] == 'before') ? $shopCurrency['symbol'] . $d->price : $d->price . " " . $shopCurrency['symbol'];
            $content .= '</div>';
        }

        $content .= '</div>';

        $content .= '</div>';

    }


    $content .= '</div>';

    $content .= '</div>';

} else {
    $content .= '<div class="section group">';
    $content .= '<div class="e360-grid e360-grid-pad">';
    $content .= '<h3>Sorry, there are no items to display for this category</h3>';
    $content .= '</div></div>';

}
